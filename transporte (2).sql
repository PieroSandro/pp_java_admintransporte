-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2019 a las 05:54:47
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `transporte`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxanio` (IN `sanio` VARCHAR(4))  begin
select idboleta,consignatario,total,fecha,unidad,b.idformapago,fp.numerodocumento from boleta b, formapago fp where b.idformapago=fp.idformapago and year(fecha)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxanioformapago` (`sanio` VARCHAR(4))  BEGIN select idboleta, fp.idformapago,fp.numerodocumento from boleta, formapago fp where boleta.idformapago=fp.idformapago and year(fecha)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxaniomes` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  begin
select idboleta,consignatario,total,fecha,
unidad,b.idformapago,fp.numerodocumento from boleta b, formapago fp where b.idformapago=fp.idformapago 
and (year(fecha)=sanio and month(fecha)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxaniomesformapago` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  BEGIN select idboleta, fp.idformapago,fp.numerodocumento from boleta, formapago fp 
where boleta.idformapago=fp.idformapago and (year(fecha)=sanio and month(fecha)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxconsignatario` (`sconsignatario` VARCHAR(20))  begin
select idboleta,consignatario,b.total,b.fecha,
b.unidad,b.idformapago,fp.numerodocumento from boleta b, formapago fp where b.idformapago=fp.idformapago and consignatario=sconsignatario order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxconsignatariodocumento` (`sconsignatario` VARCHAR(20))  BEGIN select idboleta, fp.idformapago,fp.numerodocumento from boleta, formapago fp where boleta.idformapago=fp.idformapago and consignatario=sconsignatario order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxfecha` (`sd1` DATE, `sd2` DATE)  begin select idboleta,consignatario,total,fecha, unidad,b.idformapago,fp.numerodocumento from boleta b, formapago fp where b.idformapago=fp.idformapago and (fecha BETWEEN sd1 AND sd2) ORDER BY 1 ASC; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletaxfechaformapago` (`sd1` DATE, `sd2` DATE)  begin select idboleta,fp.idformapago,fp.numerodocumento from boleta b, formapago fp where b.idformapago=fp.idformapago and (fecha BETWEEN sd1 AND sd2) ORDER BY 1 ASC; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxanio` (IN `sanio` VARCHAR(4))  begin
select idboleto,fechaemision,posicionboleto,b.oficina,
b.valor,b.descuento,b.idcliente,c.nombre from boleto b, cliente c where b.idcliente=c.idcliente and year(fechaemision)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxaniocliente` (`sanio` VARCHAR(4))  BEGIN select idboleto, c.idcliente,c.nombre from boleto, cliente c where boleto.idcliente=c.idcliente and year(fechaemision)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxaniomes` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  begin
select idboleto,fechaemision,posicionboleto,b.oficina,
b.valor,b.descuento,b.idcliente,c.nombre from boleto b, cliente c where b.idcliente=c.idcliente 
and (year(fechaemision)=sanio and month(fechaemision)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxaniomescliente` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  BEGIN select idboleto, c.idcliente,c.nombre from boleto, cliente c where boleto.idcliente=c.idcliente 
and (year(fechaemision)=sanio and month(fechaemision)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxfecha` (`sd1` DATE, `sd2` DATE)  begin
select idboleto,fechaemision,posicionboleto,b.oficina,
b.valor,b.descuento,b.idcliente,c.nombre from boleto b, cliente c where b.idcliente=c.idcliente 
and (fechaemision BETWEEN sd1 AND sd2)
ORDER BY 1 ASC;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxfechacliente` (`sd1` DATE, `sd2` DATE)  begin select idboleto,c.idcliente,c.nombre from boleto b, cliente c where b.idcliente=c.idcliente and (fechaemision BETWEEN sd1 AND sd2) ORDER BY 1 ASC; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxoficina` (`soficina` VARCHAR(45))  begin
select idboleto,fechaemision,posicionboleto,b.oficina,
b.valor,b.descuento,b.idcliente,c.nombre from boleto b, cliente c where b.idcliente=c.idcliente and b.oficina=soficina order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `boletoxoficinacliente` (`soficina` VARCHAR(45))  BEGIN select idboleto, c.idcliente,c.nombre from boleto, cliente c where boleto.idcliente=c.idcliente and boleto.oficina=soficina order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboletaxdatosformapago` (`sidfp` INT(9))  BEGIN SELECT idboleta,fp.idformapago,fp.numerodocumento from boleta, formapago fp where boleta.idformapago=fp.idformapago and fp.idformapago=sidfp; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboletaxformapago` (`sidfp` INT(9))  BEGIN SELECT idboleta,consignatario,total,fecha,unidad,boleta.idformapago from boleta, formapago fp where boleta.idformapago=fp.idformapago and boleta.idformapago=sidfp order by 1 asc; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboleto` (`sidboleto` INT(9))  BEGIN
SELECT idboleto, fechaemision, posicionboleto, oficina,valor,descuento,idcliente from boleto where idboleto=sidboleto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboletocliente` (`sidboleto` INT(9))  BEGIN SELECT idboleto,c.idcliente,c.nombre from boleto, cliente c where boleto.idcliente=c.idcliente and idboleto=sidboleto; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboletoxcliente` (IN `sidcliente` INT(9))  BEGIN SELECT idboleto,fechaemision,posicionboleto,oficina,valor,boleto.descuento,
boleto.idcliente,c.nombre
from boleto, cliente c
where boleto.idcliente=c.idcliente and 
boleto.idcliente=sidcliente order by 1 asc; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarboletoxdatoscliente` (IN `sidcliente` INT(9))  BEGIN SELECT idboleto,c.idcliente,c.nombre from boleto, cliente c where boleto.idcliente=c.idcliente and c.idcliente=sidcliente; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarclienteid` (IN `sid` INT(9))  BEGIN
SELECT idcliente,c.nombre,nacionalidad,tipodocumento,
numerodocumento,valorpasaje,descuento,numeroacomodacion,s.nombre
from cliente c,servicio s
where c.idservicio=s.idservicio and idcliente=sid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarclientenom` (IN `snom` VARCHAR(45))  BEGIN
SELECT idcliente,c.nombre,nacionalidad,tipodocumento,
numerodocumento,valorpasaje,descuento,numeroacomodacion,s.nombre
from cliente c,servicio s
where c.idservicio=s.idservicio and c.nombre=snom;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscardetallevehiculo` (`siddv` INT(9))  BEGIN select iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca,tara,carga,metros, valor, descuento,idreserva from detallevehiculo where iddetallevehiculo=siddv; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarfacturaxdatosformapago` (`sidfp` INT(9))  BEGIN SELECT idfactura,fp.idformapago,fp.numerodocumento from factura, formapago fp where factura.idformapago=fp.idformapago and fp.idformapago=sidfp; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarfacturaxformapago` (`sidfp` INT(9))  BEGIN SELECT idfactura,tipofactura,fechafactura,neto,iva,total,factura.idformapago from factura, formapago fp where factura.idformapago=fp.idformapago and factura.idformapago=sidfp order by 1 asc; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarformapagoid` (`sid` INT(9))  BEGIN
SELECT idformapago,tipodocumento,numerodocumento,entidademisora,rucgirador,cantidad,fechadocumento,nota
from formapago
where idformapago=sid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaritinerario` (`sidit` INT(9))  begin select iditinerario,fechaitinerario,
horasalida,nombrelocacion
from locacion,itinerario 
where locacion.idlocaciones=itinerario.idlocaciones and iditinerario=sidit; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarlocacion` (`sidl` INT(9))  BEGIN select idlocaciones,nombrelocacion,estado,
operador, observaciones from locacion where idlocaciones=sidl; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarreserva` (`sidreserva` INT(9))  BEGIN
SELECT idreserva, fechageneracion, estadoreserva, fechaexpiracion,valor,descuento,contactoreserva,fonocontacto,email,tr.nombre,idcliente from reserva, tiporeserva tr where reserva.idtiporeserva=tr.idtiporeserva and idreserva=sidreserva;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarreservacliente` (`sidreserva` INT(9))  BEGIN SELECT idreserva,c.idcliente,c.nombre,c.nacionalidad,c.tipodocumento,c.numerodocumento from reserva, cliente c where reserva.idcliente=c.idcliente and idreserva=sidreserva; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarreservaxcliente` (`sidcliente` INT(9))  BEGIN SELECT idreserva,fechageneracion,estadoreserva,fechaexpiracion,valor,descuento,
contactoreserva,
fonocontacto,email,tr.nombre, idcliente
from reserva, tiporeserva tr
where reserva.idtiporeserva=tr.idtiporeserva and 
idcliente=sidcliente order by 1 asc; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscartreserva` (`sidtiporeserva` INT(9))  BEGIN
SELECT idtiporeserva, tiempodeexpiracion, nombre, clasificacion from tiporeserva where idtiporeserva=sidtiporeserva;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarusuario` (IN `slogin` VARCHAR(20), IN `spassw` VARCHAR(20), IN `ses` VARCHAR(20))  BEGIN 
select login,passw,estado
from usuario
where login=slogin and passw=spassw and estado=ses;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarusuarionom` (`snombre` VARCHAR(45))  BEGIN SELECT ruc, nombre, login, passw,estado,o.nombreoficina from usuario, oficina o where usuario.idoficina=o.idoficina and nombre=snombre; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarusuarioruc` (`sruc` BIGINT(20))  BEGIN SELECT ruc, nombre, login, passw,estado,o.nombreoficina from usuario, oficina o where usuario.idoficina=o.idoficina and ruc=sruc; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `detallevehiculoxanio` (`sanio` VARCHAR(20))  begin
select iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca,tara,carga,metros,valor,descuento,idreserva from detallevehiculo where agno=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `detallevehiculoxaniomarca` (`sanio` VARCHAR(20), `smarca` VARCHAR(20))  begin
select iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca,tara,carga,metros,valor,descuento,idreserva from detallevehiculo where (agno=sanio and marca=smarca) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `detallevehiculoxmarca` (`smarca` VARCHAR(20))  begin select iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca,tara,carga,metros,valor,descuento,idreserva from detallevehiculo where marca=smarca order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `detallevehiculoxtipovehiculo` (`stv` VARCHAR(20))  begin
select iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca,tara,carga,metros,valor,descuento,idreserva from detallevehiculo where tipovehiculo=stv order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarboleto` (`sidb` INT(9))  BEGIN
Delete from boleto where idboleto=sidb;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarcliente` (`sid` INT(9))  BEGIN
Delete from cliente where idcliente=sid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminardetallevehiculo` (`siddv` INT(9))  BEGIN delete from detallevehiculo where iddetallevehiculo=siddv; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminaritinerario` (`sidi` INT(9))  BEGIN delete from itinerario where iditinerario=sidi; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarlocacion` (`sidl` INT(9))  BEGIN
delete from locacion where idlocaciones=sidl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminartreserva` (`sidtiporeserva` INT(9))  BEGIN
delete from tiporeserva where idtiporeserva=sidtiporeserva;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxanio` (`sanio` VARCHAR(4))  begin select idfactura,tipofactura,fechafactura,neto,iva,total,f.idformapago,fp.numerodocumento from factura f, formapago fp where f.idformapago=fp.idformapago and year(fechafactura)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxanioformapago` (`sanio` VARCHAR(4))  BEGIN select idfactura, fp.idformapago,fp.numerodocumento from factura, formapago fp where factura.idformapago=fp.idformapago and year(fechafactura)=sanio order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxaniomes` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  begin select idfactura,tipofactura,fechafactura,neto,iva,total,f.idformapago,fp.numerodocumento from factura f, formapago fp where f.idformapago=fp.idformapago and (year(fechafactura)=sanio and month(fechafactura)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxaniomesformapago` (`sanio` VARCHAR(4), `smes` VARCHAR(2))  BEGIN select idfactura, fp.idformapago,fp.numerodocumento from factura, formapago fp where factura.idformapago=fp.idformapago and (year(fechafactura)=sanio and month(fechafactura)=smes) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxfecha` (`sd1` DATE, `sd2` DATE)  begin select idfactura,tipofactura,fechafactura,neto, iva,total,f.idformapago,fp.numerodocumento from factura f, formapago fp where f.idformapago=fp.idformapago and (fechafactura BETWEEN sd1 AND sd2) ORDER BY 1 ASC; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxfechaformapago` (`sd1` DATE, `sd2` DATE)  begin select idfactura,fp.idformapago,fp.numerodocumento from factura f, formapago fp where f.idformapago=fp.idformapago and (fechafactura BETWEEN sd1 AND sd2) ORDER BY 1 ASC; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxtipofactura` (`stfactura` VARCHAR(20))  begin select idfactura,tipofactura,f.fechafactura,f.neto,f.iva,f.total,f.idformapago,fp.numerodocumento from factura f, formapago fp where f.idformapago=fp.idformapago and tipofactura=stfactura order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facturaxtipofacturadocumento` (`stfactura` VARCHAR(20))  BEGIN select idfactura, fp.idformapago,fp.numerodocumento from factura, formapago fp where factura.idformapago=fp.idformapago and tipofactura=stfactura order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarboleto` (`sfe` DATE, `spb` INT(9), `so` VARCHAR(45), `svalor` DOUBLE(5,2), `sdescuento` DOUBLE(5,2), `sidcli` INT(9))  BEGIN 
INSERT INTO boleto (idboleto, fechaemision, posicionboleto, oficina, valor, descuento, idcliente) 
VALUES (null,sfe,spb,so,svalor,sdescuento,sidcli); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarcliente` (`snombre` VARCHAR(45), `snacionalidad` VARCHAR(20), `stipodocumento` VARCHAR(10), `snumerodocumento` INT(9), `svalorpasaje` DOUBLE, `sdescuento` DOUBLE, `snumeroacomodacion` INT(4), `snombres` VARCHAR(45))  BEGIN Declare sidser int(9);
set sidser = (select idservicio from servicio where nombre = snombres); 
INSERT INTO cliente (idcliente,nombre, nacionalidad, tipodocumento, numerodocumento, valorpasaje, descuento, numeroacomodacion, idservicio) 
VALUES (null,snombre,snacionalidad,stipodocumento,snumerodocumento,svalorpasaje,sdescuento,snumeroacomodacion,sidser); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabardetallevehiculo` (`sp` VARCHAR(20), `stv` VARCHAR(20), `sc` VARCHAR(20), `sa` VARCHAR(20), `sm` VARCHAR(20), `st` VARCHAR(20), `sca` VARCHAR(20), `sme` DOUBLE(5,2), `sv` DOUBLE(5,2), `sd` DOUBLE(5,2), `idr` INT(9))  BEGIN insert into detallevehiculo(iddetallevehiculo,patente,tipovehiculo,consignatario,agno,marca, tara,carga,metros,valor,descuento,idreserva)values(null,sp,stv,sc,sa,sm,st,sca,sme,sv,sd,idr); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarfactura` (`stf` VARCHAR(20), `sff` DATETIME, `sneto` DOUBLE(5,2), `siva` DOUBLE(5,2), `stotal` DOUBLE(5,2), `sidforpa` INT(9))  BEGIN
INSERT INTO factura (idfactura, tipofactura, fechafactura, neto, iva, total, idformapago) VALUES (null,stf,sff,sneto,siva,stotal,sidforpa); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarformapago` (IN `stp` VARCHAR(20), IN `snd` VARCHAR(10), IN `see` VARCHAR(20), IN `srg` BIGINT(11), IN `sc` INT(9), IN `sfd` DATETIME, IN `sn` VARCHAR(20))  BEGIN
insert into formapago(idformapago,tipodocumento,numerodocumento,entidademisora,rucgirador,cantidad,                    fechadocumento,nota)values(null,stp,snd,see,srg,sc,sfd,sn);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabaritinerario` (IN `sfi` DATETIME, IN `shs` TIME, IN `snl` VARCHAR(20))  BEGIN 
Declare sidl int(9);
set sidl = (select idlocaciones from locacion where nombrelocacion = snl);
INSERT INTO itinerario (iditinerario, fechaitinerario, horasalida, idlocaciones) 
VALUES (null,sfi,shs,sidl); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarlocacion` (`snl` VARCHAR(20), `se` VARCHAR(20), `sop` VARCHAR(20), `sob` VARCHAR(20))  BEGIN insert into locacion(idlocaciones,nombrelocacion,estado,operador, observaciones)values(null,snl,se,sop,sob); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarreserva` (IN `sfg` DATE, IN `ser` VARCHAR(20), IN `sfe` DATETIME, IN `svalor` DOUBLE(5,2), IN `sdescuento` DOUBLE(5,2), IN `scr` VARCHAR(20), IN `sfc` VARCHAR(10), IN `semail` VARCHAR(20), IN `snombretr` VARCHAR(20), IN `sidcli` INT(9))  BEGIN 
Declare sidtr int(9);
set sidtr = (select idtiporeserva from tiporeserva where nombre = snombretr);
INSERT INTO reserva (idreserva, fechageneracion, estadoreserva, fechaexpiracion, valor, descuento, contactoreserva, fonocontacto, email, idtiporeserva, idcliente) 
VALUES (null,sfg,ser,sfe,svalor,sdescuento,scr,sfc,semail,sidtr,sidcli); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabartreserva` (IN `ste` DATE, IN `snombre` VARCHAR(20), IN `sclasi` VARCHAR(20))  BEGIN
insert into tiporeserva(idtiporeserva,tiempodeexpiracion,nombre,clasificacion)values(null,ste,snombre,sclasi);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grabarusuario` (`sruc` BIGINT(20), `snombre` VARCHAR(45), `slogin` VARCHAR(20), `spassw` VARCHAR(20), `sestado` VARCHAR(20), `snoficina` VARCHAR(20))  BEGIN
Declare sido int(9);
set sido = (select idoficina from oficina where nombreoficina = snoficina);
INSERT INTO usuario (ruc, nombre, login, passw, estado, idoficina) 
VALUES (sruc,snombre,slogin,spassw,sestado,sido); END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listaranioboleto` ()  BEGIN select year(fechaemision) from boleto
group by year(fechaemision) order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarboleta` ()  BEGIN select * from boleta order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarboletadocumento` ()  BEGIN select idboleta, fp.idformapago,fp.numerodocumento from boleta, formapago fp where boleta.idformapago=fp.idformapago order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarboleto` ()  BEGIN select * from boleto order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarboletocliente` ()  BEGIN select idboleto,
c.idcliente,c.nombre from boleto, cliente c 
where boleto.idcliente=c.idcliente
order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarcliente` ()  BEGIN
SELECT idcliente, c.nombre, nacionalidad, tipodocumento, numerodocumento, valorpasaje, descuento, numeroacomodacion, s.nombre
from cliente c, servicio s
where c.idservicio=s.idservicio order by 1 asc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listardetallevehiculo` ()  begin select * from detallevehiculo order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarfactura` ()  BEGIN select * from factura order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarfacturadocumento` ()  BEGIN select idfactura, fp.idformapago,fp.numerodocumento from factura, formapago fp where factura.idformapago=fp.idformapago order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarformapago` ()  BEGIN select * from formapago order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listaritinerario` ()  begin select iditinerario,fechaitinerario,
horasalida,nombrelocacion
from locacion,itinerario 
where locacion.idlocaciones=itinerario.idlocaciones order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarlocacion` ()  begin select * from locacion order by 1 asc;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarlocacionnombre` ()  BEGIN select nombrelocacion from locacion order by nombrelocacion asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listaroficinanombre` ()  BEGIN select nombreoficina from oficina order by nombreoficina asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarreserva` ()  BEGIN
SELECT idreserva, fechageneracion, estadoreserva, fechaexpiracion, valor, descuento, contactoreserva,fonocontacto,email, tr.nombre, idcliente
from reserva, tiporeserva tr
where reserva.idtiporeserva=tr.idtiporeserva order by 1 asc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarservicio` ()  BEGIN
select idservicio, nombre from servicio order by 2 asc;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listartreserva` ()  BEGIN
select * from tiporeserva order by 1 asc;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listartreservanombre` ()  BEGIN select nombre from tiporeserva order by nombre asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarusuario` ()  BEGIN select ruc,nombre,login,passw,estado,o.nombreoficina from usuario u, oficina o where u.idoficina=o.idoficina order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarusuariooficina` ()  BEGIN select u.ruc, o.idoficina,o.nombreoficina from usuario u, oficina o where u.idoficina=o.idoficina order by 1 asc; end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `locacionxnombre` (`snom` VARCHAR(20))  BEGIN
select idlocaciones,nombrelocacion,estado,
operador,observaciones from locacion
where nombrelocacion like concat(snom,'%')
order by 2 asc;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificarboleto` (`sidbol` INT(9), `sfe` DATE, `spb` INT(9), `so` VARCHAR(45), `svalor` DOUBLE(5,2), `sdescuento` DOUBLE(5,2), `sidcli` INT(9))  BEGIN
update boleto SET fechaemision=sfe, posicionboleto=spb,oficina=so,valor=svalor,descuento=sdescuento,idcliente=sidcli where idboleto = sidbol;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificarcliente` (`sidcliente` INT(9), `snombre` VARCHAR(45), `snacionalidad` VARCHAR(20), `stipodocumento` VARCHAR(10), `snumerodocumento` INT(9), `svalorpasaje` DOUBLE, `sdescuento` DOUBLE, `snumeroacomodacion` INT(4), `snombres` VARCHAR(45))  BEGIN Declare sidser int(9);
set sidser = (select idservicio from servicio where nombre = snombres); 
UPDATE cliente set nombre=snombre, nacionalidad=snacionalidad, tipodocumento=stipodocumento, numerodocumento=snumerodocumento, valorpasaje=svalorpasaje, descuento=sdescuento, numeroacomodacion=snumeroacomodacion, idservicio=sidser where idcliente=sidcliente; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificardetallevehiculo` (`siddv` INT(9), `sp` VARCHAR(20), `stv` VARCHAR(20), `sc` VARCHAR(20), `sa` VARCHAR(20), `sm` VARCHAR(20), `st` VARCHAR(20), `sca` VARCHAR(20), `sme` DOUBLE(5,2), `sv` DOUBLE(5,2), `sd` DOUBLE(5,2), `idr` INT(9))  BEGIN update detallevehiculo SET patente=sp, tipovehiculo=stv, consignatario=sc,agno=sa,marca=sm,tara=st,carga=sca,
metros=sme,valor=sv,descuento=sd,idreserva=idr where iddetallevehiculo = siddv; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificaritinerario` (IN `sit` INT(9), IN `sfi` DATETIME, IN `shs` TIME, IN `snl` VARCHAR(20))  BEGIN 
Declare sidl int(9);
set sidl = (select idlocaciones from locacion where nombrelocacion = snl);
UPDATE itinerario set fechaitinerario=sfi,horasalida=shs,
idlocaciones=sidl where iditinerario=sit; END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificarlocacion` (`sidl` INT(9), `snl` VARCHAR(20), `se` VARCHAR(20), `sop` VARCHAR(20), `sob` VARCHAR(20))  BEGIN update locacion SET nombrelocacion=snl, estado=se, operador=sop,observaciones=sob where idlocaciones = sidl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificartreserva` (`sidtiporeserva` INT(9), `ste` DATE, `snombre` VARCHAR(20), `sclasi` VARCHAR(20))  BEGIN
update tiporeserva SET tiempodeexpiracion=ste, nombre=snombre, clasificacion=sclasi where idtiporeserva = sidtiporeserva;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE `boleta` (
  `idboleta` int(9) NOT NULL,
  `consignatario` varchar(20) NOT NULL,
  `total` double(5,2) NOT NULL,
  `fecha` date NOT NULL,
  `unidad` double(5,2) NOT NULL,
  `idformapago` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`idboleta`, `consignatario`, `total`, `fecha`, `unidad`, `idformapago`) VALUES
(1, 'yo', 5.00, '2018-12-03', 7.00, 2),
(2, 'tu', 7.00, '2018-12-26', 9.00, 5),
(3, 'el', 7.00, '2019-01-08', 9.00, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleto`
--

CREATE TABLE `boleto` (
  `idboleto` int(9) NOT NULL,
  `fechaemision` date NOT NULL,
  `posicionboleto` int(9) NOT NULL,
  `oficina` varchar(45) NOT NULL,
  `valor` double(5,2) NOT NULL,
  `descuento` double(5,2) NOT NULL,
  `idcliente` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boleto`
--

INSERT INTO `boleto` (`idboleto`, `fechaemision`, `posicionboleto`, `oficina`, `valor`, `descuento`, `idcliente`) VALUES
(1, '2018-11-02', 5, 'f', 7.70, 7.00, 26),
(6, '2018-12-02', 2, '4', 5.00, 3.00, 29),
(8, '2018-12-03', 101, '102', 103.00, 104.00, 26),
(9, '2018-12-02', 41, '42', 43.00, 44.00, 27),
(10, '2018-12-03', 51, '52', 53.00, 54.00, 26),
(11, '2019-01-16', 65, 'y', 6.00, 7.90, 26),
(12, '2020-02-04', 67, 't', 8.00, 9.30, 25),
(13, '2020-03-04', 67, 'u', 5.00, 9.80, 26),
(14, '2018-12-20', 4, 'rt', 6.00, 2.00, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(9) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `nacionalidad` varchar(20) NOT NULL,
  `tipodocumento` varchar(10) NOT NULL,
  `numerodocumento` int(9) NOT NULL,
  `valorpasaje` double NOT NULL,
  `descuento` double NOT NULL,
  `numeroacomodacion` int(4) NOT NULL,
  `idservicio` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombre`, `nacionalidad`, `tipodocumento`, `numerodocumento`, `valorpasaje`, `descuento`, `numeroacomodacion`, `idservicio`) VALUES
(24, 'f', 'b', 'd', 8, 3.2, 9.6, 8, 3434),
(25, 'q', 'w', 'e', 3, 4, 5.7, 3, 3434),
(26, 'cu', 'ft', 'er', 5, 5.6, 3.4, 8, 7878),
(27, 'f', 'b', 'd', 8, 3.2, 9.6, 8, 3434),
(28, 'f', 'b', 'd', 8, 3.2, 9.6, 8, 3434),
(29, 'luis', 's', 'd', 6, 4.3, 2.3, 6, 3434);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallevehiculo`
--

CREATE TABLE `detallevehiculo` (
  `iddetallevehiculo` int(9) NOT NULL,
  `patente` varchar(20) NOT NULL,
  `tipovehiculo` varchar(20) NOT NULL,
  `consignatario` varchar(20) NOT NULL,
  `agno` varchar(20) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `tara` varchar(20) NOT NULL,
  `carga` varchar(20) NOT NULL,
  `metros` double(5,2) NOT NULL,
  `valor` double(5,2) NOT NULL,
  `descuento` double(5,2) NOT NULL,
  `idreserva` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detallevehiculo`
--

INSERT INTO `detallevehiculo` (`iddetallevehiculo`, `patente`, `tipovehiculo`, `consignatario`, `agno`, `marca`, `tara`, `carga`, `metros`, `valor`, `descuento`, `idreserva`) VALUES
(1, 'a', 'Bus', 'b', '2018', 'c', 'd', 'e', 5.00, 377.00, 56.00, 460),
(2, 'a', 'Bus', 'b', '2018', 'c', 'd', 'e', 5.00, 377.00, 56.00, 460),
(3, 'qa', 'qe', 'qi', '2019', 'qo', 'qu', 'ka', 3.00, 6.00, 8.00, 463),
(5, 'ya', 'ye', 'yi', '2017', 'yo', 'yu', 'po', 7.90, 5.00, 0.30, 457),
(6, 'lu', 'Bus', 'lo', '2016', 'yo', 'li', 'le', 6.50, 3.00, 8.90, 455);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idfactura` int(9) NOT NULL,
  `tipofactura` varchar(20) NOT NULL,
  `fechafactura` date NOT NULL,
  `neto` double(5,2) NOT NULL,
  `iva` double(5,2) NOT NULL,
  `total` double(5,2) NOT NULL,
  `idformapago` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formapago`
--

CREATE TABLE `formapago` (
  `idformapago` int(9) NOT NULL,
  `tipodocumento` varchar(20) NOT NULL,
  `numerodocumento` varchar(10) NOT NULL,
  `entidademisora` varchar(20) NOT NULL,
  `rucgirador` bigint(11) NOT NULL,
  `cantidad` int(9) NOT NULL,
  `fechadocumento` datetime NOT NULL,
  `nota` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formapago`
--

INSERT INTO `formapago` (`idformapago`, `tipodocumento`, `numerodocumento`, `entidademisora`, `rucgirador`, `cantidad`, `fechadocumento`, `nota`) VALUES
(1, 'g', '099', 'yo', 4555, 33, '2018-11-24 04:19:00', 't'),
(2, '2', '3', '5', 4, 6, '2018-11-28 16:56:09', '7'),
(3, '1', '2', '4', 3, 5, '2018-11-28 16:59:26', '6'),
(4, '1', '2', '4', 3, 5, '2018-11-28 17:00:19', '6'),
(5, 'ga', '1', '3', 2, 4, '2018-11-28 17:03:05', '5'),
(6, 'johnu', '1', '22', 55, 34, '2018-11-28 17:07:57', '44'),
(7, '4', '5', '7', 6, 8, '2018-11-30 14:39:25', '9'),
(8, '11', '12', '14', 13, 15, '2018-11-30 14:40:15', '16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerario`
--

CREATE TABLE `itinerario` (
  `iditinerario` int(9) NOT NULL,
  `fechaitinerario` datetime NOT NULL,
  `horasalida` time NOT NULL,
  `idlocaciones` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `itinerario`
--

INSERT INTO `itinerario` (`iditinerario`, `fechaitinerario`, `horasalida`, `idlocaciones`) VALUES
(1, '2018-12-10 05:22:08', '07:16:13', 2),
(5, '2018-12-10 05:22:08', '07:16:13', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaprecio`
--

CREATE TABLE `listaprecio` (
  `idlista` int(9) NOT NULL,
  `valorlista` double NOT NULL,
  `idservicio` int(9) NOT NULL,
  `nombre` int(9) DEFAULT NULL,
  `idlocaciones` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locacion`
--

CREATE TABLE `locacion` (
  `idlocaciones` int(9) NOT NULL,
  `nombrelocacion` varchar(20) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `operador` varchar(20) NOT NULL,
  `observaciones` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `locacion`
--

INSERT INTO `locacion` (`idlocaciones`, `nombrelocacion`, `estado`, `operador`, `observaciones`) VALUES
(2, 'pisco', 'd', 'f', 'g'),
(5, 'loreto', 'z', 'x', 'c');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficina`
--

CREATE TABLE `oficina` (
  `idoficina` int(9) NOT NULL,
  `nombreoficina` varchar(20) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `ciudad` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `jefeoficinas` varchar(20) NOT NULL,
  `clasificacion` varchar(20) NOT NULL,
  `observaciones` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oficina`
--

INSERT INTO `oficina` (`idoficina`, `nombreoficina`, `direccion`, `ciudad`, `telefono`, `jefeoficinas`, `clasificacion`, `observaciones`, `email`) VALUES
(1, 'admin', 'ba', 'be', '3444', 'bi', 'bo', 'bu', 'er'),
(2, 'usu', 'ca', 'ce', '34456', 'ci', 'co', 'cu', 'dca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `idreserva` int(9) NOT NULL,
  `fechageneracion` date NOT NULL,
  `estadoreserva` varchar(20) NOT NULL,
  `fechaexpiracion` datetime NOT NULL,
  `valor` double(5,2) NOT NULL,
  `descuento` double(5,2) NOT NULL,
  `contactoreserva` varchar(20) NOT NULL,
  `fonocontacto` varchar(10) NOT NULL,
  `email` varchar(20) NOT NULL,
  `idtiporeserva` int(9) DEFAULT NULL,
  `idcliente` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`idreserva`, `fechageneracion`, `estadoreserva`, `fechaexpiracion`, `valor`, `descuento`, `contactoreserva`, `fonocontacto`, `email`, `idtiporeserva`, `idcliente`) VALUES
(455, '2018-11-06', 'ft', '2018-11-07 03:12:08', 55.00, 7.00, '3444', '34', 'yu', 49, 26),
(456, '2018-11-01', 'no', '2018-11-22 03:11:04', 4.50, 8.20, 'fe', '21', 'ty', 48, 27),
(457, '2018-10-24', 'h', '2020-11-07 03:12:08', 4.50, 3.20, '22', '65', 'ga', 42, 24),
(458, '2018-11-21', 'f', '2018-11-14 04:00:00', 7.70, 8.90, 'ii', '55', 'l', 42, 26),
(460, '2018-10-24', 'h', '2020-11-07 03:12:08', 4.50, 3.20, '22', '65', 'ga', 48, 24),
(462, '2018-10-24', 'h', '2020-11-07 03:12:08', 4.50, 3.20, '22', '65', 'ga', 42, 24),
(463, '2018-10-24', 'h', '2020-11-07 03:12:08', 4.50, 3.20, '22', '65', 'ga', 42, 24),
(466, '2018-10-24', 'h', '2020-11-07 03:12:08', 4.50, 3.20, '22', '65', 'ga', 42, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `idservicio` int(9) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `tiposervicio` varchar(45) NOT NULL,
  `subtiposervicio` varchar(45) NOT NULL,
  `unidadventa` double(5,2) NOT NULL,
  `butacaservicio` int(11) NOT NULL,
  `informaservicio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`idservicio`, `nombre`, `tiposervicio`, `subtiposervicio`, `unidadventa`, `butacaservicio`, `informaservicio`) VALUES
(3434, 'a', 'b', 'c', 5.60, 0, 'd'),
(7878, 'excursión', 'y', 'h', 8.30, 3, 'u');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--

CREATE TABLE `temporada` (
  `nombre` int(9) NOT NULL,
  `mesinicio` varchar(45) NOT NULL,
  `dia` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporeserva`
--

CREATE TABLE `tiporeserva` (
  `idtiporeserva` int(9) NOT NULL,
  `tiempodeexpiracion` date NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `clasificacion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiporeserva`
--

INSERT INTO `tiporeserva` (`idtiporeserva`, `tiempodeexpiracion`, `nombre`, `clasificacion`) VALUES
(41, '2018-11-15', '45', '9'),
(42, '2019-11-23', 'tiburcio', 'gomez'),
(47, '2019-02-10', 'mike', 'tyson'),
(48, '2019-01-23', 'pulache', 'mexicodc'),
(49, '2018-11-23', 'ter', 'mexicodc'),
(52, '2018-11-11', 'hu', 'ki'),
(53, '2018-12-13', 'hg', 'rt'),
(54, '2018-12-21', 'b', 'n'),
(55, '2018-12-22', 'io', 'hi'),
(56, '2018-12-27', 'co', 'cos'),
(57, '2018-12-29', 'tec', 'pi');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ruc` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `login` varchar(20) NOT NULL,
  `passw` varchar(20) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `idoficina` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ruc`, `nombre`, `login`, `passw`, `estado`, `idoficina`) VALUES
(56, 'hj', 'sd', 'uu', 'Usuario', 2),
(5667, 'lg', 'y1', 'y2', 'ad', 1),
(8888, 'a', 'a1', 'a2', 'df', 2),
(233323331, 'hj', 't1', 't2', 'Usuario', 2),
(2345678901, 'p', 'p2', 'p3', 'Administrador', 1),
(12345678901, 'n', 'n2', 'n3', 'Administrador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(9) NOT NULL,
  `fecha` date NOT NULL,
  `valor` double NOT NULL,
  `fechapago` datetime NOT NULL,
  `estado` varchar(20) NOT NULL,
  `ruc` bigint(20) NOT NULL,
  `idboleta` int(9) DEFAULT NULL,
  `idfactura` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD PRIMARY KEY (`idboleta`),
  ADD KEY `idformapago` (`idformapago`);

--
-- Indices de la tabla `boleto`
--
ALTER TABLE `boleto`
  ADD PRIMARY KEY (`idboleto`),
  ADD KEY `idcliente` (`idcliente`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD KEY `idservicio` (`idservicio`);

--
-- Indices de la tabla `detallevehiculo`
--
ALTER TABLE `detallevehiculo`
  ADD PRIMARY KEY (`iddetallevehiculo`),
  ADD KEY `idreserva` (`idreserva`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idfactura`),
  ADD KEY `idformapago` (`idformapago`);

--
-- Indices de la tabla `formapago`
--
ALTER TABLE `formapago`
  ADD PRIMARY KEY (`idformapago`);

--
-- Indices de la tabla `itinerario`
--
ALTER TABLE `itinerario`
  ADD PRIMARY KEY (`iditinerario`),
  ADD KEY `idlocaciones` (`idlocaciones`);

--
-- Indices de la tabla `listaprecio`
--
ALTER TABLE `listaprecio`
  ADD PRIMARY KEY (`idlista`),
  ADD KEY `idservicio` (`idservicio`),
  ADD KEY `listaprecio_ibfk_3` (`idlocaciones`),
  ADD KEY `listaprecio_ibfk_2` (`nombre`);

--
-- Indices de la tabla `locacion`
--
ALTER TABLE `locacion`
  ADD PRIMARY KEY (`idlocaciones`);

--
-- Indices de la tabla `oficina`
--
ALTER TABLE `oficina`
  ADD PRIMARY KEY (`idoficina`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`idreserva`),
  ADD KEY `idcliente` (`idcliente`),
  ADD KEY `idtiporeserva` (`idtiporeserva`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`idservicio`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`nombre`);

--
-- Indices de la tabla `tiporeserva`
--
ALTER TABLE `tiporeserva`
  ADD PRIMARY KEY (`idtiporeserva`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ruc`),
  ADD KEY `idoficina` (`idoficina`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `ruc` (`ruc`),
  ADD KEY `venta_ibfk_2` (`idboleta`),
  ADD KEY `venta_ibfk_3` (`idfactura`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boleta`
--
ALTER TABLE `boleta`
  MODIFY `idboleta` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `boleto`
--
ALTER TABLE `boleto`
  MODIFY `idboleto` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `detallevehiculo`
--
ALTER TABLE `detallevehiculo`
  MODIFY `iddetallevehiculo` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idfactura` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `formapago`
--
ALTER TABLE `formapago`
  MODIFY `idformapago` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `itinerario`
--
ALTER TABLE `itinerario`
  MODIFY `iditinerario` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `listaprecio`
--
ALTER TABLE `listaprecio`
  MODIFY `idlista` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `locacion`
--
ALTER TABLE `locacion`
  MODIFY `idlocaciones` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `oficina`
--
ALTER TABLE `oficina`
  MODIFY `idoficina` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `idreserva` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=467;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `idservicio` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7879;
--
-- AUTO_INCREMENT de la tabla `temporada`
--
ALTER TABLE `temporada`
  MODIFY `nombre` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tiporeserva`
--
ALTER TABLE `tiporeserva`
  MODIFY `idtiporeserva` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` int(9) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD CONSTRAINT `boleta_ibfk_1` FOREIGN KEY (`idformapago`) REFERENCES `formapago` (`idformapago`);

--
-- Filtros para la tabla `boleto`
--
ALTER TABLE `boleto`
  ADD CONSTRAINT `boleto_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`idservicio`) REFERENCES `servicio` (`idservicio`);

--
-- Filtros para la tabla `detallevehiculo`
--
ALTER TABLE `detallevehiculo`
  ADD CONSTRAINT `detallevehiculo_ibfk_1` FOREIGN KEY (`idreserva`) REFERENCES `reserva` (`idreserva`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`idformapago`) REFERENCES `formapago` (`idformapago`);

--
-- Filtros para la tabla `itinerario`
--
ALTER TABLE `itinerario`
  ADD CONSTRAINT `itinerario_ibfk_1` FOREIGN KEY (`idlocaciones`) REFERENCES `locacion` (`idlocaciones`);

--
-- Filtros para la tabla `listaprecio`
--
ALTER TABLE `listaprecio`
  ADD CONSTRAINT `listaprecio_ibfk_2` FOREIGN KEY (`nombre`) REFERENCES `temporada` (`nombre`),
  ADD CONSTRAINT `listaprecio_ibfk_3` FOREIGN KEY (`idlocaciones`) REFERENCES `locacion` (`idlocaciones`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`idtiporeserva`) REFERENCES `tiporeserva` (`idtiporeserva`),
  ADD CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`),
  ADD CONSTRAINT `reserva_ibfk_3` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`),
  ADD CONSTRAINT `reserva_ibfk_4` FOREIGN KEY (`idtiporeserva`) REFERENCES `tiporeserva` (`idtiporeserva`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idoficina`) REFERENCES `oficina` (`idoficina`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`ruc`) REFERENCES `usuario` (`ruc`),
  ADD CONSTRAINT `venta_ibfk_2` FOREIGN KEY (`idboleta`) REFERENCES `boleta` (`idboleta`),
  ADD CONSTRAINT `venta_ibfk_3` FOREIGN KEY (`idfactura`) REFERENCES `factura` (`idfactura`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
