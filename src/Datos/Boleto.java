/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.util.Date;

/**
 *
 * @author PIERO
 */
public class Boleto {
    private int idboleto;
    private Date fe;
    private int pb;
    private String oficina;
    private double valor;
    private double descuento;
    private int idcliente;

    public Boleto(){
        
    }
    
    public Boleto(int idboleto, Date fe, int pb, String oficina, double valor, double descuento, int idcliente) {
        this.idboleto = idboleto;
        this.fe = fe;
        this.pb = pb;
        this.oficina = oficina;
        this.valor = valor;
        this.descuento = descuento;
        this.idcliente = idcliente;
    }

    public int getIdboleto() {
        return idboleto;
    }

    public void setIdboleto(int idboleto) {
        this.idboleto = idboleto;
    }

    public Date getFe() {
        return fe;
    }

    public void setFe(Date fe) {
        this.fe = fe;
    }

    public int getPb() {
        return pb;
    }

    public void setPb(int pb) {
        this.pb = pb;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }
    
    
}
