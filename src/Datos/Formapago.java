/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.util.Date;

/**
 *
 * @author PIERO
 */
public class Formapago {
    private int idformapago;
    private String tdocumento;
    private String ndocumento;
    private String eemisora;
    private long rgirador;
    private int cantidad;
    private Date fdocumento;
    
    private String nota;

    public Formapago() {
    }
    
    public Formapago(int idformapago, String tdocumento, String ndocumento, String eemisora, long rgirador, int cantidad, Date fdocumento, String nota) {
        this.idformapago = idformapago;
        this.tdocumento = tdocumento;
        this.ndocumento = ndocumento;
        this.eemisora = eemisora;
        this.rgirador = rgirador;
        this.cantidad = cantidad;
        this.fdocumento = fdocumento;
        this.nota = nota;
    }

    public int getIdformapago() {
        return idformapago;
    }

    public void setIdformapago(int idformapago) {
        this.idformapago = idformapago;
    }

    public String getTdocumento() {
        return tdocumento;
    }

    public void setTdocumento(String tdocumento) {
        this.tdocumento = tdocumento;
    }

    public String getNdocumento() {
        return ndocumento;
    }

    public void setNdocumento(String ndocumento) {
        this.ndocumento = ndocumento;
    }

    public String getEemisora() {
        return eemisora;
    }

    public void setEemisora(String eemisora) {
        this.eemisora = eemisora;
    }

    public long getRgirador() {
        return rgirador;
    }

    public void setRgirador(long rgirador) {
        this.rgirador = rgirador;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFdocumento() {
        return fdocumento;
    }

    public void setFdocumento(Date fdocumento) {
        this.fdocumento = fdocumento;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
    
    
    
}
