/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.util.Date;
/**
 *
 * @author PIERO
 */
public class Reserva {
    private int idreserva;
    private Date fg;
    private String ereserva;
    private Date fe;
    private double valor;
    private double descuento;
    private String creserva;
    private String fc;
    private String email;
    private String tiporeserva;
    private int idcli;
   
    

    public Reserva() {
    }

    public Reserva(int idreserva, Date fg, String ereserva,
        Date fe, double valor, double descuento, String creserva,
        String fc, String email, String tiporeserva,int idcli
    ) {
        this.idreserva = idreserva;
        this.fg = fg;
        this.ereserva = ereserva;
        this.fe = fe;
        this.valor = valor;
        this.descuento = descuento;
        this.creserva = creserva;
        this.fc = fc;
        this.email = email;
        this.tiporeserva = tiporeserva;
        this.idcli = idcli;
        
    }

    public int getIdreserva() {
        return idreserva;
    }

    public void setIdreserva(int idreserva) {
        this.idreserva = idreserva;
    }

    public Date getFg() {
        return fg;
    }

    public void setFg(Date fg) {
        this.fg = fg;
    }

    public String getEreserva() {
        return ereserva;
    }

    public void setEreserva(String ereserva) {
        this.ereserva = ereserva;
    }

    public Date getFe() {
        return fe;
    }

    public void setFe(Date fe) {
        this.fe = fe;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public String getCreserva() {
        return creserva;
    }

    public void setCreserva(String creserva) {
        this.creserva = creserva;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTiporeserva() {
        return tiporeserva;
    }

    public void setTiporeserva(String tiporeserva) {
        this.tiporeserva = tiporeserva;
    }

    public int getIdcli() {
        return idcli;
    }

    public void setIdcli(int idcli) {
        this.idcli = idcli;
    }
    
    
}
