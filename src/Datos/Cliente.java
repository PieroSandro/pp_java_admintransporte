
package Datos;

public class Cliente {
    private int idcliente;
    private String nombre;
    private String nacionalidad;
    private String tipodocumento;
    private int numerodocumento;
    private double valorpasaje;
    
    private double descuento;
    private int numacomodacion;
    private String servicio;
    

    public Cliente() {
    }

    public Cliente(int idcliente, String nombre, String nacionalidad,
    String tipodocumento, int numerodocumento,double valorpasaje,
    double descuento,int numacomodacion, String servicio       
    ) {
        this.idcliente = idcliente;
        this.nombre = nombre;
        this.nacionalidad = nacionalidad;
        this.tipodocumento = tipodocumento;
        this.numerodocumento=numerodocumento;
        this.valorpasaje=valorpasaje;
        this.descuento=descuento;
        this.numacomodacion=numacomodacion;
        this.servicio=servicio;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public int getNumerodocumento() {
        return numerodocumento;
    }

    public void setNumerodocumento(int numerodocumento) {
        this.numerodocumento = numerodocumento;
    }

    public double getValorpasaje() {
        return valorpasaje;
    }

    public void setValorpasaje(double valorpasaje) {
        this.valorpasaje = valorpasaje;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public int getNumacomodacion() {
        return numacomodacion;
    }

    public void setNumacomodacion(int numacomodacion) {
        this.numacomodacion = numacomodacion;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

  
    
    
    
    
}
