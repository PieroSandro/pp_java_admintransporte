/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author PIERO
 */
public class Servicio {
    private int idservicio;
    private String nombre;
    private String tiposervicio;
    private String subtiposervicio;
    private double unidadventa;
    private int butacaservicio;
    private String informaservicio;
    
    public Servicio() {
    }

    public Servicio(int idservicio, String nombre,String tiposervicio,
            String subtiposervicio,double unidadventa,
            int butacaservicio,String informaservicio) {
        this.idservicio = idservicio;
        this.nombre = nombre;
        this.tiposervicio = tiposervicio;
        this.subtiposervicio = subtiposervicio;
        this.unidadventa=unidadventa;
        this.butacaservicio=butacaservicio;
        this.informaservicio=informaservicio;
        
    }

    public int getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(int idservicio) {
        this.idservicio = idservicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(String tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    public String getSubtiposervicio() {
        return subtiposervicio;
    }

    public void setSubtiposervicio(String subtiposervicio) {
        this.subtiposervicio = subtiposervicio;
    }

    public double getUnidadventa() {
        return unidadventa;
    }

    public void setUnidadventa(double unidadventa) {
        this.unidadventa = unidadventa;
    }

    public int getButacaservicio() {
        return butacaservicio;
    }

    public void setButacaservicio(int butacaservicio) {
        this.butacaservicio = butacaservicio;
    }

    public String getInformaservicio() {
        return informaservicio;
    }

    public void setInformaservicio(String informaservicio) {
        this.informaservicio = informaservicio;
    }
    
    
}
