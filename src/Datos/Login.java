
package Datos;


public class Login {
    private String usuario;
    private String contraseña;
    private String estado;

    public Login() {
    }

    public Login(String us, String ps, String es) {
        usuario = us;
        contraseña = ps;
        estado=es;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    
    
}
