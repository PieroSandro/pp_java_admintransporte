
package Datos;

import java.util.Date;

public class Tiporeserva {
    private int idtiporeserva;
    private Date te;
    private String nombre;
    private String clasificacion;
    
    

    public Tiporeserva() {
    }

    public Tiporeserva(int idtiporeserva, Date te, String nombre,
        String clasificacion
    ) {
        this.idtiporeserva = idtiporeserva;
        this.te = te;
        this.nombre = nombre;
        this.clasificacion = clasificacion;
        
    }

    public int getIdtiporeserva() {
        return idtiporeserva;
    }

    public void setIdtiporeserva(int idtiporeserva) {
        this.idtiporeserva = idtiporeserva;
    }

    public Date getTe() {
        return te;
    }

    public void setTe(Date te) {
        this.te = te;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    
    
}
