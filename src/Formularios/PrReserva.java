package Formularios;


import Datos.Cliente;
import Datos.Reserva;
import Datos.Tiporeserva;
import java.sql.*;

//import Otros.Limpiar;
//import Procesos.P_Producto;
import Procesos.*;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class PrReserva extends javax.swing.JFrame {

    Date dt1;
    /*ArrayList<Producto> lista = new ArrayList();
    
    Producto p;
    File archivo=new File("Proyecto.txt");
    
    FileWriter escribir;
    PrintWriter linea;*/
    

    public PrReserva() {
        //rp = new P_Producto();
        initComponents();

        
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPu1 = new javax.swing.JTextField();
        jButton10 = new javax.swing.JButton();
        plpo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        txtPu2 = new javax.swing.JTextField();
        jButton12 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtPu = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPu3 = new javax.swing.JTextField();
        txtPu4 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPu5 = new javax.swing.JTextField();
        txtPu6 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        txtPu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu1ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton10.setText("Grabar");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        plpo.setBackground(new java.awt.Color(51, 153, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 51));
        jLabel2.setText("Datos Producto");

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/exit.png"))); // NOI18N
        jButton5.setText("Salir");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Fecha generación", "Estado reserva", "Fecha expiración", "Valor", "Descuento"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);

        jLabel8.setText("Total:");

        txtPu2.setEditable(false);
        txtPu2.setBackground(new java.awt.Color(204, 204, 255));
        txtPu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu2ActionPerformed(evt);
            }
        });

        jButton12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/circlepie.png"))); // NOI18N
        jButton12.setText("Tipo Reserva");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtrar"));

        jPanel3.setBackground(new java.awt.Color(0, 153, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del cliente"));

        txtPu.setEditable(false);
        txtPu.setBackground(new java.awt.Color(204, 204, 255));
        txtPu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuActionPerformed(evt);
            }
        });

        jLabel5.setText("ID");

        jLabel6.setText("Nacionalidad");

        jLabel7.setText("TD");

        txtPu3.setEditable(false);
        txtPu3.setBackground(new java.awt.Color(204, 204, 255));
        txtPu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu3ActionPerformed(evt);
            }
        });

        txtPu4.setEditable(false);
        txtPu4.setBackground(new java.awt.Color(204, 204, 255));
        txtPu4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu4ActionPerformed(evt);
            }
        });

        jLabel9.setText("Nombre");

        jLabel10.setText("Número documento");

        txtPu5.setEditable(false);
        txtPu5.setBackground(new java.awt.Color(204, 204, 255));
        txtPu5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu5ActionPerformed(evt);
            }
        });

        txtPu6.setBackground(new java.awt.Color(204, 204, 255));
        txtPu6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPu4, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(28, 28, 28)
                                .addComponent(txtPu, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(27, 27, 27)
                                .addComponent(txtPu3, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(159, 159, 159)
                                .addComponent(jLabel9)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPu5, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPu6, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtPu3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtPu5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel10)
                    .addComponent(txtPu6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtPu4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jRadioButton1.setBackground(new java.awt.Color(0, 153, 255));
        jRadioButton1.setText("Todos");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jRadioButton2.setBackground(new java.awt.Color(0, 153, 255));
        jRadioButton2.setText("Específico");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jRadioButton2)
                            .addComponent(jRadioButton1))
                        .addGap(11, 11, 11)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jRadioButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jRadioButton2)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jButton1.setText("Más detalles");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout plpoLayout = new javax.swing.GroupLayout(plpo);
        plpo.setLayout(plpoLayout);
        plpoLayout.setHorizontalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 679, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, plpoLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 136, Short.MAX_VALUE))))
            .addGroup(plpoLayout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(235, 235, 235)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jButton12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton5)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        plpoLayout.setVerticalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton12)
                    .addComponent(jButton5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(plpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    private void txtPuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuActionPerformed

    /*private void formWindowOpened(java.awt.event.WindowEvent evt) {                                  
        // cargando el combo de marcas
        /*List<Marca> lista = new ArrayList(); 
        Acceso ac = new Acceso();
        lista = ac.listar_marcas();//listarmarcas es de ac de acceso
        jComboBox1.addItem("--Seleccione--");
        for (int i=0; i<lista.size();i++){
            jComboBox1.addItem(lista.get(i).getNombre());//en el combo va el nombre
        }*/
           
       
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Menu m = new Menu();
            this.dispose();
            m.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

                              
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
         jRadioButton1.setSelected(true);
        
        jComboBox1.addItem("--Seleccione--");
        
        jComboBox1.addItem("Búsqueda por ID de reserva");
        jComboBox1.addItem("Búsqueda por ID de cliente");
        
        jRadioButton2.setSelected(false);
        jComboBox1.setEnabled(false);
        jTextField1.setEnabled(false);
        jButton4.setEnabled(false);
        
        modeloTabla.addColumn("Código");
           modeloTabla.addColumn("Fecha generación");
           modeloTabla.addColumn("Estado de reserva");
           modeloTabla.addColumn("Fecha expiración");
           modeloTabla.addColumn("Valor");
           modeloTabla.addColumn("Descuento");
           
           //ASIGNAR MOIDELO A LA TABLA
           tabla.setModel(modeloTabla);
           
           modeloTabla.setRowCount(0);
        List<Reserva> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listarreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdreserva());
            fila[1]=String.valueOf(lista.get(i).getFg());
            fila[2]=lista.get(i).getEreserva();
            fila[3]=String.valueOf(lista.get(i).getFe());
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
         
         jButton1.setEnabled(false);
    }//GEN-LAST:event_formWindowOpened

    private void txtPu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu1ActionPerformed

    private void txtPu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu2ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        Tireserva tr = new Tireserva();
            this.dispose();
            tr.setVisible(true);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        String s1=jTextField1.getText();
          
          String buscar = String.valueOf(jComboBox1.getSelectedItem());
        
        
         Cliente c = new Cliente();
         Reserva r=new Reserva();
        Acceso ac = new Acceso();
        
         
        if(buscar.equals("Búsqueda por ID de reserva")){
            if(s1.matches("[a-zA-Z_]+")) {
       JOptionPane.showMessageDialog(null, "El ID debe ser solo en números");
                return;
    }
            int is1=Integer.parseInt(s1);
        c = ac.buscarreservacliente(is1);
        r = ac.buscarreserva(is1);
        if(c.getNombre() != null){
            
            txtPu3.setText(String.valueOf(c.getIdcliente()));
            txtPu5.setText(c.getNombre());
            txtPu.setText(c.getNacionalidad());
            txtPu6.setText(String.valueOf(c.getNumerodocumento()));
            txtPu4.setText(c.getTipodocumento());
            
            
             modeloTabla.setRowCount(0);
        int total=0;
        
            fila[0]=String.valueOf(r.getIdreserva());
            fila[1]=String.valueOf(r.getFg());
            fila[2]=r.getEreserva();
            fila[3]=String.valueOf(r.getFe());
            fila[4]=String.valueOf(r.getValor());
            fila[5]=String.valueOf(r.getDescuento());
            modeloTabla.addRow(fila);
            total++;
        
         txtPu2.setText(""+(total));  
         
          jButton1.setEnabled(false);
        }
        else
            JOptionPane.showMessageDialog(null,"ID de reserva no existe");
         jButton1.setEnabled(false);
        }else if(buscar.equals("Búsqueda por ID de cliente")){
            int is2=Integer.parseInt(s1);
            c = ac.buscarclienteid(is2);
            List<Reserva> lista = new ArrayList();
            //---------Acceso ac2=new Acceso();
            lista = ac.buscarreservaxcliente(is2);
        if(c.getNombre() != null){
            //if(r.getIdcli() != 0){
            //Acceso ac = new Acceso();
        //lista = ac.listarreserva();
        txtPu3.setText(String.valueOf(c.getIdcliente()));
            txtPu5.setText(c.getNombre());
            txtPu.setText(c.getNacionalidad());
            txtPu6.setText(String.valueOf(c.getNumerodocumento()));
            txtPu4.setText(c.getTipodocumento());
        modeloTabla.setRowCount(0);
        int total=0;
        
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdreserva());
            fila[1]=String.valueOf(lista.get(i).getFg());
            fila[2]=lista.get(i).getEreserva();
            fila[3]=String.valueOf(lista.get(i).getFe());
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            modeloTabla.addRow(fila);
            total++;
        }
        
         txtPu2.setText(""+(total));  
         
         jButton1.setEnabled(false);
                
        
        
        }
        else
            JOptionPane.showMessageDialog(null,"ID de cliente no existe");
        
        }else{
            JOptionPane.showMessageDialog(null,"Seleccione el tipo de búsqueda");
       
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void txtPu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu3ActionPerformed

    private void txtPu4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu4ActionPerformed

    private void txtPu5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu5ActionPerformed

    private void txtPu6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu6ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        modeloTabla.setRowCount(0);
        jRadioButton2.setSelected(false);
        jComboBox1.setEnabled(false);
        jTextField1.setEnabled(false);
        jButton4.setEnabled(false);
        
        
           
           modeloTabla.setRowCount(0);
        List<Reserva> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listarreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdreserva());
            fila[1]=String.valueOf(lista.get(i).getFg());
            fila[2]=lista.get(i).getEreserva();
            fila[3]=String.valueOf(lista.get(i).getFe());
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
         
         jButton1.setEnabled(false);
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        jRadioButton1.setSelected(false);
        jComboBox1.setEnabled(true);
        jTextField1.setEnabled(true);
        jButton4.setEnabled(true);
        
           
           modeloTabla.setRowCount(0);
           txtPu2.setText(""+0);  
         
         jButton1.setEnabled(false);
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int selectedRowIndex=tabla.getSelectedRow();
        
        int id= Integer.parseInt(modeloTabla.getValueAt(selectedRowIndex,0).toString());
        Reserva r = new Reserva();
        
        
        
       
        Acceso ac = new Acceso();
        r = ac.buscarreserva(id);
        //if (tr.getNombre() !=null)
        
            JOptionPane.showMessageDialog(null,"Id: " + r.getIdreserva() + "\nTiempo de expiración: " + r.getCreserva() + "\nNombre: " + r.getFc() + "\nClasificación: " + r.getEmail()+"\n "+r.getTiporeserva()+"\n "+r.getIdcli());
        //else
            //.showMessageDialog(null, "Producto No Grabado...");
                    
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked
        
        
        int selectedRowIndex=tabla.getSelectedRow();
        if(selectedRowIndex!=-1){
            jButton1.setEnabled(true);
        };
    }//GEN-LAST:event_tablaMouseClicked

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrReserva().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPanel plpo;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtPu;
    private javax.swing.JTextField txtPu1;
    private javax.swing.JTextField txtPu2;
    private javax.swing.JTextField txtPu3;
    private javax.swing.JTextField txtPu4;
    private javax.swing.JTextField txtPu5;
    private javax.swing.JTextField txtPu6;
    // End of variables declaration//GEN-END:variables
    DefaultTableModel modeloTabla = new DefaultTableModel();
    //declarando la fila del modelo
    String fila[] = new String[6];
 
    }


