package Formularios;


import Datos.Cliente;
import Datos.Tiporeserva;
import java.sql.*;

//import Otros.Limpiar;
//import Procesos.P_Producto;
import Procesos.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.print.*;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.Date;  
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class Tireserva extends javax.swing.JFrame {

   
    /*ArrayList<Producto> lista = new ArrayList();
    
    Producto p;
    File archivo=new File("Proyecto.txt");
    
    FileWriter escribir;
    PrintWriter linea;*/
    Date dt1;

    public Tireserva() {
        //rp = new P_Producto();
        initComponents();
        
        //-----------------------------------------+1
        /*try {
            cargar_txt();
            listar();
        } catch (Exception ex) {

        }*/

        /*lt.soloLetras(txtPro);
        lt.soloNumero(txtId);
        lt.soloNumero1(txtCan);
        lt.soloReal(txtPu);*/

    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPu1 = new javax.swing.JTextField();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        plpo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtPu2 = new javax.swing.JTextField();
        txtId = new javax.swing.JTextField();
        txtPu = new javax.swing.JTextField();
        txtCan = new javax.swing.JTextField();
        dateChooserCombo1 = new datechooser.beans.DateChooserCombo();
        jButton9 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();

        txtPu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu1ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton10.setText("Grabar");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search.png"))); // NOI18N
        jButton11.setText("Buscar");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        plpo.setBackground(new java.awt.Color(51, 153, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 51));
        jLabel2.setText("Tipos de reserva");

        jLabel3.setText("Id");

        jLabel4.setText("Producto");

        jLabel5.setText("Cantidad");

        jLabel6.setText("P.U.");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ViewType_List.png"))); // NOI18N
        jButton2.setText("Listar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search.png"))); // NOI18N
        jButton3.setText("Buscar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete.png"))); // NOI18N
        jButton4.setText("Eliminar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/exit.png"))); // NOI18N
        jButton5.setText("Salir");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Tiempo de expiración", "Nombre", "Clasificación"
            }
        ));
        jScrollPane1.setViewportView(tabla);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/modify.png"))); // NOI18N
        jButton6.setText("Modificar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton7.setText("Grabar");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/new.png"))); // NOI18N
        jButton8.setText("Nuevo");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jLabel8.setText("Total");

        txtPu2.setEditable(false);
        txtPu2.setBackground(new java.awt.Color(204, 204, 255));
        txtPu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu2ActionPerformed(evt);
            }
        });

        txtId.setEditable(false);
        txtId.setBackground(new java.awt.Color(204, 204, 255));
        txtId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdActionPerformed(evt);
            }
        });

        txtPu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuActionPerformed(evt);
            }
        });

        txtCan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCanActionPerformed(evt);
            }
        });

        dateChooserCombo1.setCurrentView(new datechooser.view.appearance.AppearancesList("Swing",
            new datechooser.view.appearance.ViewAppearance("custom",
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    true,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 255),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(128, 128, 128),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(255, 0, 0),
                    false,
                    false,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                (datechooser.view.BackRenderer)null,
                false,
                true)));

    jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/modify.png"))); // NOI18N
    jButton9.setText("Imprimir");
    jButton9.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton9ActionPerformed(evt);
        }
    });

    jButton1.setText("Grafica 1");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton1ActionPerformed(evt);
        }
    });

    jButton12.setText("Grafica 2");
    jButton12.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton12ActionPerformed(evt);
        }
    });

    jButton13.setText("Generar pdf");
    jButton13.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton13ActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout plpoLayout = new javax.swing.GroupLayout(plpo);
    plpo.setLayout(plpoLayout);
    plpoLayout.setHorizontalGroup(
        plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(plpoLayout.createSequentialGroup()
            .addGap(37, 37, 37)
            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(plpoLayout.createSequentialGroup()
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
                .addGroup(plpoLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(plpoLayout.createSequentialGroup()
                            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(plpoLayout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dateChooserCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, plpoLayout.createSequentialGroup()
                                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel6))
                                    .addGap(29, 29, 29)
                                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtPu)
                                        .addComponent(txtCan)))
                                .addGroup(plpoLayout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(62, 62, 62)
                                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addGap(31, 31, 31)
                            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButton7)
                                .addComponent(jButton8)
                                .addComponent(jButton2)
                                .addComponent(jButton5))
                            .addGap(42, 42, 42)
                            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButton9)
                                .addGroup(plpoLayout.createSequentialGroup()
                                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButton4)
                                        .addComponent(jButton3)
                                        .addComponent(jButton6, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addGap(60, 60, 60)
                                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButton1)
                                        .addComponent(jButton12)
                                        .addComponent(jButton13))))
                            .addGap(79, 79, 79))
                        .addGroup(plpoLayout.createSequentialGroup()
                            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 642, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(plpoLayout.createSequentialGroup()
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
    );
    plpoLayout.setVerticalGroup(
        plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(plpoLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(plpoLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel4)
                        .addComponent(dateChooserCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtCan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(txtPu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(plpoLayout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton3)
                        .addComponent(jButton2)
                        .addComponent(jButton1))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton4)
                        .addComponent(jButton8)
                        .addComponent(jButton12))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton6)
                        .addComponent(jButton7)
                        .addComponent(jButton13))
                    .addGap(18, 18, 18)
                    .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton5)
                        .addComponent(jButton9))))
            .addGap(31, 31, 31)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(48, 48, 48)
            .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel8)
                .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(plpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    private void txtPuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuActionPerformed

    private void txtCanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCanActionPerformed

    private void txtIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        txtId.setText(null);
       
        txtCan.setText(null);
        txtPu.setText(null);
        
    }//GEN-LAST:event_jButton8ActionPerformed

    /*private void formWindowOpened(java.awt.event.WindowEvent evt) {                                  
        // cargando el combo de marcas
        /*List<Marca> lista = new ArrayList(); 
        Acceso ac = new Acceso();
        lista = ac.listar_marcas();//listarmarcas es de ac de acceso
        jComboBox1.addItem("--Seleccione--");
        for (int i=0; i<lista.size();i++){
            jComboBox1.addItem(lista.get(i).getNombre());//en el combo va el nombre
        }*/
           
       
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
//dateChooserCombo1.setDateFormatString("yyyy-MM-dd");
        //String d=Integer.toString(dateChooserCombo1.get(Calendar.DAY_OF_MONTH));
        String d=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.DAY_OF_MONTH));
       String m=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.MONTH)+1);
       String a=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.YEAR));
       String fecha=(a+"-"+m+"-"+d);
        
       
       String t2 = txtCan.getText();
        String t3 = txtPu.getText();
        
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
        //DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        try {
            //DateFormat df=new SimpleDateFormat("dd/MM/YY");
            dt1=df.parse(fecha);
        } catch (ParseException ex) {
            Logger.getLogger(Tireserva.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tiporeserva tr = new Tiporeserva();
        
        tr.setTe(dt1);
        tr.setNombre(t2);
        tr.setClasificacion(t3);
        
        
         Acceso ac1 = new Acceso();
        int resp = ac1.grabartreserva(dt1, t2, t3);
        if (resp ==1){
             modeloTabla.setRowCount(0);
        List<Tiporeserva> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listartreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdtiporeserva());
            fila[1]=String.valueOf(lista.get(i).getTe());
            fila[2]=lista.get(i).getNombre();
            fila[3]=lista.get(i).getClasificacion();;
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
        
            JOptionPane.showMessageDialog(null, "Grabado..."+
                    fila[0]+","+tr.getNombre());
            }
        else
        {JOptionPane.showMessageDialog(null, "Producto No Grabado...");}
                    
                
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        
        int selectedRowIndex=tabla.getSelectedRow();
        
        int id= Integer.parseInt(modeloTabla.getValueAt(selectedRowIndex,0).toString());
        
        String d=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.DAY_OF_MONTH));
       String m=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.MONTH)+1);
       String a=Integer.toString(dateChooserCombo1.getCurrent().get(Calendar.YEAR));
       String fecha=(a+"-"+m+"-"+d);
        String t2 = txtCan.getText();
        String t3 = txtPu.getText();
        
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
        //DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        try {
            //DateFormat df=new SimpleDateFormat("dd/MM/YY");
            dt1=df.parse(fecha);
        } catch (ParseException ex) {
            Logger.getLogger(Tireserva.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tiporeserva tr = new Tiporeserva();
        
        tr.setIdtiporeserva(id);
        tr.setTe(dt1);
        tr.setNombre(t2);
        tr.setClasificacion(t3);
       
        Acceso ac = new Acceso();
        int resp = ac.modificartreserva(id, dt1, t2, t3);
        if (resp ==1){
        modeloTabla.setRowCount(0);
        List<Tiporeserva> lista = new ArrayList();
        Acceso ac2 = new Acceso();
        lista = ac2.listartreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdtiporeserva());
            fila[1]=String.valueOf(lista.get(i).getTe());
            fila[2]=lista.get(i).getNombre();
            fila[3]=lista.get(i).getClasificacion();;
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
            JOptionPane.showMessageDialog(null, "Producto Modificado");
        //else
            //.showMessageDialog(null, "Producto No Grabado...");
                    }else
        {JOptionPane.showMessageDialog(null, "Producto no modificado");}
          
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        PrReserva pr = new PrReserva();
            this.dispose();
            pr.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
                
        int selectedRowIndex=tabla.getSelectedRow();
        
        int id= Integer.parseInt(modeloTabla.getValueAt(selectedRowIndex,0).toString());
                 
        //String codpro=txtId.getText();
       
        
       
        Acceso ac = new Acceso();
        int resp = ac.eliminartreserva(id);
        if (resp ==1){
            modeloTabla.setRowCount(0);
        List<Tiporeserva> lista = new ArrayList();
        Acceso ac2 = new Acceso();
        lista = ac2.listartreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdtiporeserva());
            fila[1]=String.valueOf(lista.get(i).getTe());
            fila[2]=lista.get(i).getNombre();
            fila[3]=lista.get(i).getClasificacion();;
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
            JOptionPane.showMessageDialog(null, "Producto Eliminado con id: "+id);
        //else
            //.showMessageDialog(null, "Producto No Grabado...");
                    }else
        {JOptionPane.showMessageDialog(null, "Producto Eliminado");}
    
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        //buscar();
            int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el ID del tipo de reserva: "));
            
        Tiporeserva tr = new Tiporeserva();
        
        Acceso ac = new Acceso();
        tr = ac.buscartreserva(id);
        if (tr.getNombre() !=null){
        
            JOptionPane.showMessageDialog(null,"Id: " + tr.getIdtiporeserva() + "\nTiempo de expiración: " + tr.getTe() + "\nNombre: " + tr.getNombre() + "\nClasificación: " + tr.getClasificacion());
        //else
            //.showMessageDialog(null, "Producto No Grabado...");
                    }else
        {JOptionPane.showMessageDialog(null, "Producto no encontrado");}
   
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        /*
        String filePath="C:\\JavaApps_Piero\\PruebaCueto\\Ejercicio3_3\\ejercicio3.txt";
        File file = new File(filePath);
        try {
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            
             modeloTabla = (DefaultTableModel)tabla.getModel();
              Object[] lines=br.lines().toArray();
             double sumaTotalIngreso=0.0;
            for(int i=0;i<lines.length;i++){
                    
                    cabezaTabla=lines[i].toString().split(",");
                    
                    
                    modeloTabla.addRow(cabezaTabla);
                    sumaTotalIngreso=sumaTotalIngreso+Double.parseDouble((String) modeloTabla.getValueAt(i,5));
            }
            txtPu2.setText(""+sumaTotalIngreso);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tireserva.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
        modeloTabla.addColumn("ID");
           modeloTabla.addColumn("Tiempo de expiración");
           modeloTabla.addColumn("Nombre");
           modeloTabla.addColumn("Clasificación");
           
           //ASIGNAR MOIDELO A LA TABLA
           tabla.setModel(modeloTabla);
           
           modeloTabla.setRowCount(0);
        List<Tiporeserva> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listartreserva();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdtiporeserva());
            fila[1]=String.valueOf(lista.get(i).getTe());
            fila[2]=lista.get(i).getNombre();
            fila[3]=lista.get(i).getClasificacion();;
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
        
    }//GEN-LAST:event_formWindowOpened

    private void txtPu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu1ActionPerformed

    private void txtPu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu2ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        
        MessageFormat header = new MessageFormat("Reporte");
        
        MessageFormat footer = new MessageFormat("Page{0,number,integer}");
        
        try{
            tabla.print(JTable.PrintMode.NORMAL,header,footer);
        }catch(java.awt.print.PrinterException e){
            System.err.format("No se puede imprimir", e.getMessage());
        }

    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel mimodelo2;
                Grafica migrafica2=new Grafica();
                
                mimodelo2=migrafica2.agruparReserva();
                //tabla.setModel(mimodelo2);
                
                DefaultCategoryDataset dtsc2 = new DefaultCategoryDataset();
                for(int i=0; i<mimodelo2.getRowCount();i++){
                    dtsc2.setValue(Integer.parseInt(mimodelo2.getValueAt(i,1).toString()),"",mimodelo2.getValueAt(i,0).toString());
                    //dtsc2.setValue(tblTabla.getValueAt(i,0).toString(),"",Integer.parseInt(tblTabla.getValueAt(i,1).toString()));
                
                   }
                JFreeChart ch2=ChartFactory.createBarChart3D("Gráfico de barras","","Cantidad",dtsc2,PlotOrientation.VERTICAL,false,true,false);
                CategoryPlot P2=ch2.getCategoryPlot();
                P2.setRangeGridlinePaint(Color.black);
                ChartFrame chartFrm2=new ChartFrame("Estadística",ch2);
                chartFrm2.setVisible(true);
                chartFrm2.setSize(500,400);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        DefaultTableModel mimodelo;
                Grafica migrafica=new Grafica();
                
                mimodelo=migrafica.agruparReserva();
                //tabla.setModel(mimodelo);
                
                DefaultPieDataset dtsc = new DefaultPieDataset();
                for(int i=0; i<mimodelo.getRowCount();i++){
                    dtsc.setValue(mimodelo.getValueAt(i,0).toString(),Integer.parseInt(mimodelo.getValueAt(i,1).toString()));
                
                   }
                JFreeChart ch=ChartFactory.createPieChart3D("Gráfica de barras",dtsc,true,true,false);
                PiePlot P=(PiePlot)ch.getPlot();
                ChartFrame chartFrm=new ChartFrame("Estadística",ch);
                chartFrm.setVisible(true);
                chartFrm.setSize(500,400);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        Document doc = new Document();
                try {
                    PdfWriter.getInstance(doc,new FileOutputStream ("Transporte_reserva.pdf"));
                
                    doc.open();
                    PdfPTable tbl=new PdfPTable(4);
                    
                    tbl.addCell("ID");
                    tbl.addCell("Tiempo");
                    tbl.addCell("Nombre");
                    tbl.addCell("Clasificación");
                    
                   
                    for(int i=0;i<tabla.getRowCount();i++){
                        String ID=tabla.getValueAt(i,0).toString();
                        String Tiempo=tabla.getValueAt(i,1).toString();
                        String Nombre=tabla.getValueAt(i,2).toString();
                        String Clasificacion=tabla.getValueAt(i,3).toString();
                        
                        
                        tbl.addCell(ID);
                        tbl.addCell(Tiempo);
                        tbl.addCell(Nombre);
                        tbl.addCell(Clasificacion);
                        
                    }
                    java.util.Date fecha = new java.util.Date();
                    
                    SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat sdf2=new SimpleDateFormat("HH:mm");
                    doc.add(new Paragraph("Fecha: "+sdf.format(fecha)+"\nHora: "+sdf2.format(fecha)));
                    doc.add(new Paragraph("------------------------------"));
                    doc.add(tbl);
                    JOptionPane.showMessageDialog(null, "PDF exportado");
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Tireserva.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DocumentException ex) {
                    Logger.getLogger(Tireserva.class.getName()).log(Level.SEVERE, null, ex);
                }
                   
                doc.close();
                doc.open();//-----------------------------------
    }//GEN-LAST:event_jButton13ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private datechooser.beans.DateChooserCombo dateChooserCombo1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel plpo;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtCan;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtPu;
    private javax.swing.JTextField txtPu1;
    private javax.swing.JTextField txtPu2;
    // End of variables declaration//GEN-END:variables
    DefaultTableModel modeloTabla = new DefaultTableModel();
    //declarando la fila del modelo
    String fila[] = new String[4];
 
    }


