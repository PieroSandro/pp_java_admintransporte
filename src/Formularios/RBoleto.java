package Formularios;


import Datos.Cliente;
import Datos.Reserva;
import Datos.Tiporeserva;
import Datos.Boleto;
import Datos.Formapago;
import java.sql.*;

//import Otros.Limpiar;
//import Procesos.P_Producto;
import Procesos.*;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class RBoleto extends javax.swing.JFrame {

    Date dt1;
    /*ArrayList<Producto> lista = new ArrayList();
    
    Producto p;
    File archivo=new File("Proyecto.txt");
    
    FileWriter escribir;
    PrintWriter linea;*/
    

    public RBoleto() {
        //rp = new P_Producto();
        initComponents();

        
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPu1 = new javax.swing.JTextField();
        jButton10 = new javax.swing.JButton();
        plpo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        txtPu2 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        txtPu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu1ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton10.setText("Grabar");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        plpo.setBackground(new java.awt.Color(51, 153, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("Registro de Boletos");

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Fecha Emisión", "Posición Boleto", "Oficina", "Valor", "Descuento", "Cliente"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);

        jLabel8.setText("Total:");

        txtPu2.setEditable(false);
        txtPu2.setBackground(new java.awt.Color(204, 204, 255));
        txtPu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu2ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del boleto"));

        jTextField1.setEditable(false);
        jTextField1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setText("ID");

        jLabel3.setText("Posición boleto");

        jLabel4.setText("Oficina");

        jLabel5.setText("Valor");

        jLabel6.setText("Descuento");

        jLabel7.setText("ID Cliente");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 157, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(83, 83, 83))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel3))
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4))
                .addGap(130, 130, 130))
        );

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/modify.png"))); // NOI18N
        jButton3.setToolTipText("Modificar boleto");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search.png"))); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/new.png"))); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton1.setToolTipText("Grabar boleto");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/exit.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout plpoLayout = new javax.swing.GroupLayout(plpo);
        plpo.setLayout(plpoLayout);
        plpoLayout.setHorizontalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 679, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, plpoLayout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 66, Short.MAX_VALUE))
            .addGroup(plpoLayout.createSequentialGroup()
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(jButton1)
                        .addGap(36, 36, 36)
                        .addComponent(jButton3)
                        .addGap(35, 35, 35)
                        .addComponent(jButton4)
                        .addGap(31, 31, 31)
                        .addComponent(jButton6)
                        .addGap(32, 32, 32)
                        .addComponent(jButton7)
                        .addGap(84, 84, 84)
                        .addComponent(jButton2))
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        plpoLayout.setVerticalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
                              
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
        
        modeloTabla.addColumn("Código");
           modeloTabla.addColumn("Fecha emisión");
           modeloTabla.addColumn("Posición");
           modeloTabla.addColumn("Oficina");
           modeloTabla.addColumn("Valor");
           modeloTabla.addColumn("Descuento");
           modeloTabla.addColumn("Cliente");
           
           tabla.setModel(modeloTabla);
           
           
           
           //Cliente c = new Cliente();
         //Boleto b=new Boleto();
      modeloTabla.setRowCount(0);
        List<Boleto> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listarboleto();
        
        List<Cliente> lista2 = new ArrayList();
        Acceso ac2 = new Acceso();
        lista2 = ac2.listarboletocliente();
        modeloTabla.setRowCount(0);
        
        int total=0;
        for(int i=0;i<lista.size() && i<lista2.size();i++){
             fila[0]=String.valueOf(lista.get(i).getIdboleto());
            fila[1]=String.valueOf(lista.get(i).getFe());
            fila[2]=String.valueOf(lista.get(i).getPb());
            fila[3]=lista.get(i).getOficina();
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            fila[6]=String.valueOf(lista2.get(i).getIdcliente()+" - "+lista2.get(i).getNombre());
            
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));
           
           
        /*List<Cliente> lista2 = new ArrayList();
        Acceso ac2 = new Acceso();
        lista2 = ac2.listarboletocliente();
     
        int total=0;
        for(int i=0;i<lista2.size();i++){
            
            fila[6]=String.valueOf(lista2.get(i).getIdcliente()+" - "+lista2.get(i).getNombre());
            
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total)); */ 
         
         
    }//GEN-LAST:event_formWindowOpened

    private void txtPu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu1ActionPerformed

    private void txtPu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu2ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked
        
        
        int selectedRowIndex=tabla.getSelectedRow();
        if(selectedRowIndex!=-1){
            jButton1.setEnabled(true);
        };
    }//GEN-LAST:event_tablaMouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        int selectedRowIndex=tabla.getSelectedRow();
       int id= Integer.parseInt(modeloTabla.getValueAt(selectedRowIndex,0).toString());
        
       Cliente c = new Cliente();
        Acceso ac = new Acceso();
        int resp = ac.eliminarboleto(id);
        if (resp ==1){
            modeloTabla.setRowCount(0);
        List<Boleto> lista = new ArrayList();
        Acceso ac2 = new Acceso();
        lista = ac2.listarboleto();
        
        List<Cliente> lista2 = new ArrayList();
        Acceso ac3 = new Acceso();
        lista2 = ac3.listarboletocliente();
        modeloTabla.setRowCount(0);
        int total=0;
        for(int i=0;i<lista.size() && i<lista2.size();i++){
            fila[0]=String.valueOf(lista.get(i).getIdboleto());
            fila[1]=String.valueOf(lista.get(i).getFe());
            fila[2]=String.valueOf(lista.get(i).getPb());
            fila[3]=lista.get(i).getOficina();
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            fila[6]=String.valueOf(lista2.get(i).getIdcliente()+" - "+lista2.get(i).getNombre());
            
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));  
            JOptionPane.showMessageDialog(null, "Boleto eliminado con id: "+id);
        //else
            //.showMessageDialog(null, "Producto No Grabado...");
                    }else
        {JOptionPane.showMessageDialog(null, "Boleto no eliminado");}
    
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Menu m = new Menu();
        this.dispose();
        m.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
          int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el ID del boleto: "));
           
         Cliente c = new Cliente();
         Boleto b=new Boleto();
        Acceso ac = new Acceso();
        b=ac.buscarboleto(id);
        c = ac.buscarboletocliente(id);
        
        if(c.getNombre() != null){
            
            JOptionPane.showMessageDialog(null, "Boleto encontrado"+
                    "\nID de boleto: "+b.getIdboleto()+
                    "\nFecha de emisión: "+b.getFe()+
                    "\nPosición de boleto: "+b.getPb()+
                    "\nOficina: "+b.getOficina()+
                    "\nValor: "+b.getValor()+
                    "\nDescuento: "+b.getDescuento()+
                    "\nID de cliente: "+c.getIdcliente()+
                    "\nNombre de cliente: "+c.getNombre());
        
            
        }
        else
            JOptionPane.showMessageDialog(null,"ID de boleto no existe");
         
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String t1=jTextField2.getText();
       String t2=jTextField3.getText();
       String t3=jTextField4.getText();
       
       String t4=jTextField6.getText();
       
       int it1=Integer.parseInt(t1);
       
       String t5=jTextField5.getText();
       
       double dt3=Double.parseDouble(t3);
       double dt4=Double.parseDouble(t4);
       int it5=Integer.parseInt(t5);
       
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");  
    
       Date date = new Date(); 
       String fecha=df.format(date);
        
       try {
            dt1=df.parse(fecha);
        } catch (ParseException ex) {
            Logger.getLogger(PFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
        Boleto bo = new Boleto();
        bo.setFe(dt1);
        bo.setPb(it1);
        bo.setOficina(t2);
        bo.setValor(dt3);
        bo.setDescuento(dt4);
        
        bo.setIdcliente(it5);
        Cliente c = new Cliente();
        
         Acceso ac1 = new Acceso();
        int resp = ac1.grabarboleto(dt1,it1,t2,dt3,dt4,it5);
        if (resp ==1){
        
            modeloTabla.setRowCount(0);
        List<Boleto> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listarboleto();
        
        List<Cliente> lista2 = new ArrayList();
        Acceso ac2 = new Acceso();
        lista2 = ac2.listarboletocliente();
        modeloTabla.setRowCount(0);
        
        int total=0;
        String nomcli="";
        for(int i=0;i<lista.size() && i<lista2.size();i++){
             fila[0]=String.valueOf(lista.get(i).getIdboleto());
            fila[1]=String.valueOf(lista.get(i).getFe());
            fila[2]=String.valueOf(lista.get(i).getPb());
            fila[3]=lista.get(i).getOficina();
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            fila[6]=String.valueOf(lista2.get(i).getIdcliente()+" - "+lista2.get(i).getNombre());
            nomcli=lista2.get(i).getNombre();
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));
            
        
            /*JOptionPane.showMessageDialog(null, "Boleto grabado..."+
                    fila[0]+","+tr.getNombre());*/
            
            JOptionPane.showMessageDialog(null, "Boleto grabado"+
                    "\nID de boleto: "+fila[0]+
                    "\nFecha de emisión: "+fila[1]+
                    "\nPosición de boleto: "+bo.getPb()+
                    "\nOficina: "+bo.getOficina()+
                    "\nValor: "+bo.getValor()+
                    "\nDescuento: "+bo.getDescuento()+
                    "\nID de cliente: "+bo.getIdcliente()+
                    "\nNombre de cliente: "+nomcli);//c.getNombre()
       
            }
        else
        {JOptionPane.showMessageDialog(null, "Boleto no grabado...");}
                    
   
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
        int selectedRowIndex=tabla.getSelectedRow();
        
        int id= Integer.parseInt(modeloTabla.getValueAt(selectedRowIndex,0).toString());
        
        String t1=jTextField2.getText();
       String t2=jTextField3.getText();
       String t3=jTextField4.getText();
       
       String t4=jTextField6.getText();
       
       int it1=Integer.parseInt(t1);
       
       String t5=jTextField5.getText();
       
       double dt3=Double.parseDouble(t3);
       double dt4=Double.parseDouble(t4);
       int it5=Integer.parseInt(t5);
        
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
        Date date = new Date(); 
       String fecha=df.format(date);
        
       try {
            dt1=df.parse(fecha);
        } catch (ParseException ex) {
            Logger.getLogger(PFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Boleto bo = new Boleto();
        bo.setIdboleto(id);
        bo.setFe(dt1);
        bo.setPb(it1);
        bo.setOficina(t2);
        bo.setValor(dt3);
        bo.setDescuento(dt4);
        
        bo.setIdcliente(it5);
       //----------------------------------------------------------------
       Cliente c = new Cliente();
        
         Acceso ac1 = new Acceso();
        int resp = ac1.modificarboleto(id,dt1,it1,t2,dt3,dt4,it5); 
       //Acceso ac = new Acceso();
        //int resp = ac.modificartreserva(id, dt1, t2, t3);
        if (resp ==1){
        
            modeloTabla.setRowCount(0);
        List<Boleto> lista = new ArrayList();
        Acceso ac = new Acceso();
        lista = ac.listarboleto();
        
        List<Cliente> lista2 = new ArrayList();
        Acceso ac2 = new Acceso();
        lista2 = ac2.listarboletocliente();
        modeloTabla.setRowCount(0);
        
        int total=0;
        String nomcli="";
        for(int i=0;i<lista.size() && i<lista2.size();i++){
             fila[0]=String.valueOf(lista.get(i).getIdboleto());
            fila[1]=String.valueOf(lista.get(i).getFe());
            fila[2]=String.valueOf(lista.get(i).getPb());
            fila[3]=lista.get(i).getOficina();
            fila[4]=String.valueOf(lista.get(i).getValor());
            fila[5]=String.valueOf(lista.get(i).getDescuento());
            fila[6]=String.valueOf(lista2.get(i).getIdcliente()+" - "+lista2.get(i).getNombre());
            nomcli=lista2.get(i).getNombre();
            modeloTabla.addRow(fila);
            total++;
        }
         txtPu2.setText(""+(total));
            
        
            /*JOptionPane.showMessageDialog(null, "Boleto grabado..."+
                    fila[0]+","+tr.getNombre());*/
            
            JOptionPane.showMessageDialog(null, "Boleto modificado");//c.getNombre()
       
                    }else
        {JOptionPane.showMessageDialog(null, "Boleto no modificado");}
          
    }//GEN-LAST:event_jButton3ActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RBoleto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RBoleto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RBoleto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RBoleto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RBoleto().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JPanel plpo;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtPu1;
    private javax.swing.JTextField txtPu2;
    // End of variables declaration//GEN-END:variables
    DefaultTableModel modeloTabla = new DefaultTableModel();
    //declarando la fila del modelo
    String fila[] = new String[7];
 
    }


