package Formularios;

//import Datos.Categoria;
import Datos.Cliente;
import java.sql.*;
//import Datos.Producto;
//import Otros.Limpiar;
//import Procesos.P_Producto;
import Procesos.*;
/*import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;*/
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/*import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;*/

public class FCliente extends javax.swing.JFrame {

   
    File archivo=new File("transporte.txt");
    
    FileWriter escribir;
    PrintWriter linea;
    //P_Producto rp;
    

    public FCliente() {
        //rp = new P_Producto();
        initComponents();

        //-----------------------------------------+1
        /*try {
            cargar_txt();
            listar();
        } catch (Exception ex) {

        }*/

        /*lt.soloLetras(txtPro);
        lt.soloNumero(txtId);
        lt.soloNumero1(txtCan);
        lt.soloReal(txtPu);*/

    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPu1 = new javax.swing.JTextField();
        jButton10 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        plpo = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        txtData = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        txtPu2 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();

        txtPu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu1ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save.png"))); // NOI18N
        jButton10.setText("Grabar");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jLabel9.setText("Total de clientes");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        plpo.setBackground(new java.awt.Color(51, 153, 255));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Nombre", "Nacionalidad", "Tipo Documento", "Número Documento", "Valor", "Descuento", "Número acomodación", "Servicio"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla);

        jLabel8.setText("Total de clientes");

        txtPu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPu2ActionPerformed(evt);
            }
        });

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/modify.png"))); // NOI18N
        jButton2.setToolTipText("hola muchachos");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton2MouseEntered(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/list.png"))); // NOI18N
        jButton5.setToolTipText("Información del cliente");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/shipping-red.png"))); // NOI18N
        jButton6.setToolTipText("Servicio del cliente");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/exit.png"))); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout plpoLayout = new javax.swing.GroupLayout(plpo);
        plpo.setLayout(plpoLayout);
        plpoLayout.setHorizontalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56)
                        .addComponent(jButton7))
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 711, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        plpoLayout.setVerticalGroup(
            plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(plpoLayout.createSequentialGroup()
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(plpoLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, plpoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton3)))
                .addGap(39, 39, 39)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPu2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(plpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jButton4)
                    .addComponent(jButton2)
                    .addComponent(jButton5)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(plpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        //buscar();
          String s1=txtData.getText();
          
          String buscar = String.valueOf(jComboBox1.getSelectedItem());
        
        Cliente c = new Cliente();
        Acceso ac = new Acceso();
        
         
        if(buscar.equals("Búsqueda por ID")){
            if(s1.matches("[a-zA-Z_]+")) {
       JOptionPane.showMessageDialog(null, "El ID debe ser solo en números");
                return;
    }
            int is1=Integer.parseInt(s1);
        c = ac.buscarclienteid(is1);
        if(c.getNombre() != null){
            
            
            /*cabezaTabla[0]=c.getRuc();
            cabezaTabla[1]=c.getRsocial();
            cabezaTabla[2]=c.getDireccion();
            cabezaTabla[3]=c.getTelefono();
            modeloTabla.addRow(cabezaTabla);*/
            JOptionPane.showMessageDialog(null, "ID de cliente encontrado\n"
                    + "Id: "+ c.getIdcliente()
                    + "\nNombre: "+c.getNombre()
                    + "\nNacionalidad: "+c.getNacionalidad()
                    + "\nTipo documento: "+c.getTipodocumento()
                    + "\nNúmero documento: "+c.getNumerodocumento()
                    + "\nValor pasaje: "+c.getValorpasaje()
                    + "\nDescuento: "+c.getDescuento()
                    + "\nNumero acomodación: "+c.getNumacomodacion()
                    + "\nServicio: "+c.getServicio());
                
        
        
        }
        else
            JOptionPane.showMessageDialog(null,"ID de cliente no existe");
        
        }else if(buscar.equals("Búsqueda por Nombre")){
            c = ac.buscarclientenom(s1);
        if(c.getNombre() != null){
            
            /*cabezaTabla[0]=c.getRuc();
            cabezaTabla[1]=c.getRsocial();
            cabezaTabla[2]=c.getDireccion();
            cabezaTabla[3]=c.getTelefono();
            modeloTabla.addRow(cabezaTabla);*/
            JOptionPane.showMessageDialog(null, "Nombre de cliente encontrado\n"
            + "Id: "+ c.getIdcliente()
                    + "\nNombre: "+c.getNombre()
                    + "\nNacionalidad: "+c.getNacionalidad()
                    + "\nTipo documento: "+c.getTipodocumento()
                    + "\nNúmero documento: "+c.getNumerodocumento()
                    + "\nValor pasaje: "+c.getValorpasaje()
                    + "\nDescuento: "+c.getDescuento()
                    + "\nNumero acomodación: "+c.getNumacomodacion()
                    + "\nServicio: "+c.getServicio()
            );
                
        
        
        }
        else
            JOptionPane.showMessageDialog(null,"Nombre de cliente no existe");
        
        }else{
            JOptionPane.showMessageDialog(null,"Seleccione el tipo de búsqueda");
       
        }
       
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        RCliente rc = new RCliente();
            this.dispose();
            rc.setVisible(true);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
       
	jComboBox1.addItem("--Seleccione--");
        
        jComboBox1.addItem("Búsqueda por ID");
        jComboBox1.addItem("Búsqueda por Nombre");
        
        
        modeloTabla.addColumn("ID");
           modeloTabla.addColumn("Nombre");
           modeloTabla.addColumn("Nacionalidad");
           modeloTabla.addColumn("Tipo Documento");
           modeloTabla.addColumn("Número Documento");
           modeloTabla.addColumn("Valor");
           modeloTabla.addColumn("Descuento");
           modeloTabla.addColumn("Número Acomodocación");
           modeloTabla.addColumn("Servicio");
           
           //ASIGNAR MOIDELO A LA TABLA
           tabla.setModel(modeloTabla);
           
          String filePath="C:\\JavaApps_Piero\\PruebaCueto\\TransporteCarga\\transporte.txt";
        File file = new File(filePath);
        try {
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            
             modeloTabla = (DefaultTableModel)tabla.getModel();
             //modeloTabla = new DefaultTableModel(dataTabla,cabezaTabla);
            Object[] lines=br.lines().toArray();
            //>), "menor que" (<
            int totalClientes=0;
            for(int i=0;i<lines.length;i++){
                    
                    //String line=tableLines[i].toString().trim();
                    cabezaTabla=lines[i].toString().split(",");
                    
                    
                    modeloTabla.addRow(cabezaTabla);
                    totalClientes++;
            }
            txtPu2.setText(""+(totalClientes-1));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        /*for(int i=0;i<contProducto;i++){
            sumaTotalIngreso=sumaTotalIngreso+producto[i].getPrecioTotal();
            
        
        
        /*for(int i=0;i<contProducto;i++){
            sumaTotalIngreso=sumaTotalIngreso+producto[i].getPrecioTotal();
            
        
        
        /*for(int i=0;i<contProducto;i++){
            sumaTotalIngreso=sumaTotalIngreso+producto[i].getPrecioTotal();
            
        
        
        /*for(int i=0;i<contProducto;i++){
            sumaTotalIngreso=sumaTotalIngreso+producto[i].getPrecioTotal();
            
        }*/
    }//GEN-LAST:event_formWindowOpened

    private void txtPu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu1ActionPerformed

    private void txtPu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPu2ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseEntered
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jButton2MouseEntered

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        Servicio s = new Servicio();
            this.dispose();
            s.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        Menu m = new Menu();
            this.dispose();
            m.setVisible(true);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel plpo;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtData;
    private javax.swing.JTextField txtPu1;
    private javax.swing.JTextField txtPu2;
    // End of variables declaration//GEN-END:variables
    DefaultTableModel modeloTabla = new DefaultTableModel();
    //declarando la fila del modelo
    String cabezaTabla[] = new String[4];
 
    }


