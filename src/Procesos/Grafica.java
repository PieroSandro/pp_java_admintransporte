/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Procesos;

import Procesos.Conexion;
//import static Procesos.Conexion.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PIERO
 */
public class Grafica {
    Connection con = Conexion.connect();

    public DefaultTableModel agruparReserva(){
        DefaultTableModel modelo=null;
        try{
            String titulos[]={"Clasificacion","Nombre"};
            String dts[]=new String[2];
            modelo=new DefaultTableModel(null,titulos);
            String sql="SELECT Clasificacion, count(Clasificacion) from tiporeserva group by Clasificacion";
            PreparedStatement pst=con.prepareStatement(sql);
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
                dts[0]=rs.getString("Clasificacion");
                dts[1]=rs.getString("count(Clasificacion)");
                modelo.addRow(dts);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return modelo;
    }





}
