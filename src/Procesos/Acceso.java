/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Procesos;

import java.sql.*;
import Procesos.*;
import Datos.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
/**
 *
 * @author PIERO
 */
public class Acceso extends Conexion{
    Connection con = connect();
    
    public int eliminarProducto2(String c) {
        int sw = 0;
        String sql = "call eliminarProducto2 (?)";//UN SIGNO DE INTERROGACION PORQUE SOLO PIDE CODIGO, un parametro
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, c);
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public int grabarcliente(String nom, String nacion,String td,int nd,double vp,double des,int na, String serv) {//nombres del proc almacenado
        int sw = 0;//si esta en cero no grabo nada
        String sql = "call grabarcliente (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            //ps.setInt(1, 0);//primer signo de interrogacion
            ps.setString(1, nom);//segundo signo de interrog
            ps.setString(2, nacion);
            ps.setString(3, td);
            ps.setInt(4, nd);
            ps.setDouble(5, vp);
            ps.setDouble(6, des);
            ps.setInt(7, na);
            ps.setString(8,serv);
            
            int nfilas = ps.executeUpdate();//numero de filas afectadas en nfilas... UPDATE PARA insertar, modificar
            if (nfilas > 0) {
                sw = 1;//si esta en 1, si grabo
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public int modificarProducto2(String cod, String nom, String nomcat, int cantidad, double preciounit) {
        int sw = 0;
        String sql = "call modificarProducto2 (?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, cod);//primer signo de interrogacion
            ps.setString(2, nom);//segundo signo de interrog
            ps.setString(3, nomcat);
            ps.setInt(4, cantidad);
            ps.setDouble(5, preciounit);
            
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    /*public Producto buscarProducto2(String cod) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Producto p = new Producto();//vacio
       
        String sql = "call buscarProducto2 (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, cod);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              p.setCodigo(rs.getString(1));
              p.setNombre(rs.getString(2));
              p.setCategoria(rs.getString(3));
              p.setCantidad(rs.getInt(4));
              p.setPrecioUnitario(rs.getDouble(5));
              
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return p;
    }*/
    public int buscarusuario(String us, String psw,String es) {
       int sw = 0;
       String sql = "call buscarusuario (?,?,?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, us);
            ps.setString(2, psw);
            ps.setString(3, es);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo
               sw=1;//2 por la columna q nos da el nombre del distrito
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
    }
    
     public Cliente buscarclienteid(int sid) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Cliente c = new Cliente();//vacio
       
        String sql = "call buscarclienteid (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, sid);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              c.setIdcliente(rs.getInt(1));
              c.setNombre(rs.getString(2));
              c.setNacionalidad(rs.getString(3));
              c.setTipodocumento(rs.getString(4));
              c.setNumerodocumento(rs.getInt(5));
              c.setValorpasaje(rs.getInt(6));
              c.setDescuento(rs.getDouble(7));
              c.setNumacomodacion(rs.getInt(8));
              c.setServicio(rs.getString(9));
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return c;
    }
     
     public Cliente buscarclientenom(String nom) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Cliente c = new Cliente();//vacio
       
        String sql = "call buscarclientenom (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              c.setIdcliente(rs.getInt(1));
              c.setNombre(rs.getString(2));
              c.setNacionalidad(rs.getString(3));
              c.setTipodocumento(rs.getString(4));
              c.setNumerodocumento(rs.getInt(5));
              c.setValorpasaje(rs.getInt(6));
              c.setDescuento(rs.getDouble(7));
              c.setNumacomodacion(rs.getInt(8));
              c.setServicio(rs.getString(9));
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return c;
    }
    /*
    public List<Producto> listarProducto2() {//retorna lista q contiene arreglo de distritos, no tiene inputs no necesita porq el procedmiento no tiene inputs
       List<Producto> lista = new ArrayList();//
       String sql = "call listarProducto2 ()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
                Producto p=new Producto();
                p.setCodigo(rs.getString(1));
              p.setNombre(rs.getString(2));
              p.setCategoria(rs.getString(3));
              p.setCantidad(rs.getInt(4));
              p.setPrecioUnitario(rs.getDouble(5));
              
              
              //p.setTotal(rs.getInt(3)*rs.getDouble(4));
              
               lista.add(p);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }*/
    
    
    public List<Servicio> listarservicio() {
       List<Servicio> lista = new ArrayList();//
       String sql = "call listarservicio()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Servicio s = new Servicio();
               s.setIdservicio(rs.getInt(1));//rs lee codigo
               s.setNombre(rs.getString(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               
               
               lista.add(s);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    
    public int grabartreserva(Date te, String nombre,String clasificacion) {//nombres del proc almacenado
        int sw = 0;//si esta en cero no grabo nada
        
        //SimpleDateFormat dFormat=new SimpleDateFormat("yyyy-MM-dd");
        //te=dFormat.format();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
        String ste=dateFormat.format(te);
        //String ste=te.ToString("yyyy-MM-dd");
        String sql = "call grabartreserva (?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setString(1, ste);//segundo signo de interrog
            ps.setString(2, nombre);
            ps.setString(3, clasificacion);
            
            
            int nfilas = ps.executeUpdate();//numero de filas afectadas en nfilas... UPDATE PARA insertar, modificar
            if (nfilas > 0) {
                sw = 1;//si esta en 1, si grabo
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public int grabarformapago(String td,String nd,
            String ee, long rg,int cantidad,     
            Date fd, String nota) {//nombres del proc almacenado
        int sw = 0;//si esta en cero no grabo nada
        
        //SimpleDateFormat dFormat=new SimpleDateFormat("yyyy-MM-dd");
        //te=dFormat.format();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        String sfd=dateFormat1.format(fd);
        //String ste=te.ToString("yyyy-MM-dd");
        String sql = "call grabarformapago (?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setString(1, td);
            ps.setString(2, nd);
            ps.setString(3, ee);
            ps.setLong(4,rg);
            ps.setInt(5,cantidad);
            ps.setString(6, sfd);
            ps.setString(7, nota);
            
            
            int nfilas = ps.executeUpdate();//numero de filas afectadas en nfilas... UPDATE PARA insertar, modificar
            if (nfilas > 0) {
                sw = 1;//si esta en 1, si grabo
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public int grabarreserva(Date fg, String ereserva,Date fe, double valor,
            double descuento,String creserva,String fc, String email, String tiporeserva,
            int idcli) {//nombres del proc almacenado
        int sw = 0;//si esta en cero no grabo nada
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
        String sfg=dateFormat.format(fg);
        
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        String sfe=dateFormat1.format(fe);
        //String ste=te.ToString("yyyy-MM-dd");
        String sql = "call grabarreserva (?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setString(1, sfg);//segundo signo de interrog
            ps.setString(2, ereserva);
            ps.setString(3, sfe);
            ps.setDouble(4,valor);
            ps.setDouble(5,descuento);
            ps.setString(6, creserva);
            ps.setString(7,fc);
            ps.setString(8, email);
            ps.setString(9, tiporeserva);
            ps.setInt(10,idcli);
            
            int nfilas = ps.executeUpdate();//numero de filas afectadas en nfilas... UPDATE PARA insertar, modificar
            if (nfilas > 0) {
                sw = 1;//si esta en 1, si grabo
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
     public List<Tiporeserva> listartreserva() {
       List<Tiporeserva> lista = new ArrayList();//
       String sql = "call listartreserva()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Tiporeserva tr = new Tiporeserva();
               tr.setIdtiporeserva(rs.getInt(1));//rs lee codigo
               tr.setTe(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               tr.setNombre(rs.getString(3));
               tr.setClasificacion(rs.getString(4));
               lista.add(tr);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
     
     public List<Reserva> listarreserva() {
       List<Reserva> lista = new ArrayList();//
       String sql = "call listarreserva()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Reserva r = new Reserva();
               r.setIdreserva(rs.getInt(1));//rs lee codigo
               r.setFg(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               r.setEreserva(rs.getString(3));
               r.setFe(rs.getDate(4));
               r.setValor(rs.getDouble(5));
               r.setDescuento(rs.getDouble(6));
               r.setCreserva(rs.getString(7));
               r.setFc(rs.getString(8));
               r.setEmail(rs.getString(9));
               r.setTiporeserva(rs.getString(10));
               r.setIdcli(rs.getInt(11));
               lista.add(r);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
     
     public List<Formapago> listarformapago() {
       List<Formapago> lista = new ArrayList();//
       String sql = "call listarformapago()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Formapago fp = new Formapago();
               fp.setIdformapago(rs.getInt(1));//rs lee codigo
               fp.setTdocumento(rs.getString(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               fp.setNdocumento(rs.getString(3));
               fp.setEemisora(rs.getString(4));
               fp.setRgirador(rs.getLong(5));
               fp.setCantidad(rs.getInt(6));
               fp.setFdocumento(rs.getDate(7));
               fp.setNota(rs.getString(8));
               
               lista.add(fp);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
     public List<Reserva> buscarreservaxcliente(int sid) {
       List<Reserva> lista = new ArrayList();//
       String sql = "call buscarreservaxcliente(?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1,sid);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Reserva r = new Reserva();
               r.setIdreserva(rs.getInt(1));//rs lee codigo
               r.setFg(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               r.setEreserva(rs.getString(3));
               r.setFe(rs.getDate(4));
               r.setValor(rs.getDouble(5));
               r.setDescuento(rs.getDouble(6));
               r.setCreserva(rs.getString(7));
               r.setFc(rs.getString(8));
               r.setEmail(rs.getString(9));
               r.setTiporeserva(rs.getString(10));
               r.setIdcli(rs.getInt(11));
               lista.add(r);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
     public int eliminartreserva(int tire) {
        int sw = 0;
        
        String sql = "call eliminartreserva (?)";//UN SIGNO DE INTERROGACION PORQUE SOLO PIDE CODIGO, un parametro
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, tire);
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
     
     public int modificartreserva(int cod, Date te, String nom, String clasi) {
        int sw = 0;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
        String ste=dateFormat.format(te);
        
        String sql = "call modificartreserva (?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cod);//primer signo de interrogacion
            ps.setString(2, ste);//segundo signo de interrog
            ps.setString(3, nom);
            ps.setString(4, clasi);
            
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
     
     public Tiporeserva buscartreserva(int cod) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Tiporeserva tr = new Tiporeserva();//vacio
       
        String sql = "call buscartreserva (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cod);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              
              tr.setIdtiporeserva(rs.getInt(1));//rs lee codigo
               tr.setTe(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               tr.setNombre(rs.getString(3));
               tr.setClasificacion(rs.getString(4));
              
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return tr;
    }
     
     public Reserva buscarreserva(int sid) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Reserva r = new Reserva();//vacio
       
        String sql = "call buscarreserva (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, sid);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              r.setIdreserva(rs.getInt(1));
              r.setFg(rs.getDate(2));
              r.setEreserva(rs.getString(3));
              r.setFe(rs.getDate(4));
              r.setValor(rs.getDouble(5));
              r.setDescuento(rs.getDouble(6));
              r.setCreserva(rs.getString(7));
              r.setFc(rs.getString(8));
              r.setEmail(rs.getString(9));
              r.setTiporeserva(rs.getString(10));
              r.setIdcli(rs.getInt(11));
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return r;
    }
     
     public Cliente buscarreservacliente(int sid) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Reserva r = new Reserva();//vacio
        Cliente c= new Cliente();
        String sql = "call buscarreservacliente (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, sid);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              r.setIdreserva(rs.getInt(1));
              c.setIdcliente(rs.getInt(2));
              c.setNombre(rs.getString(3));
              c.setNacionalidad(rs.getString(4));
              c.setTipodocumento(rs.getString(5));
              c.setNumerodocumento(rs.getInt(6));
              
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return c;
        //return c;
    }
     
    public int grabarboleto(Date fe, int pb,String oficina,
            double valor, double descuento, int idcliente) {//nombres del proc almacenado
        int sw = 0;//si esta en cero no grabo nada
        
        //SimpleDateFormat dFormat=new SimpleDateFormat("yyyy-MM-dd");
        //te=dFormat.format();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
        String sfe=dateFormat.format(fe);
        
        String sql = "call grabarboleto (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setString(1, sfe);//segundo signo de interrog
            ps.setInt(2, pb);
            ps.setString(3, oficina);
            ps.setDouble(4, valor);
            ps.setDouble(5, descuento);
            ps.setInt(6, idcliente);
            
            int nfilas = ps.executeUpdate();//numero de filas afectadas en nfilas... UPDATE PARA insertar, modificar
            if (nfilas > 0) {
                sw = 1;//si esta en 1, si grabo
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public List<Boleto> listarboleto() {
       List<Boleto> lista = new ArrayList();//
       String sql = "call listarboleto()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               Boleto b = new Boleto();
               b.setIdboleto(rs.getInt(1));//rs lee codigo
               
               b.setFe(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               b.setPb(rs.getInt(3));
               b.setOficina(rs.getString(4));
               b.setValor(rs.getDouble(5));
               b.setDescuento(rs.getDouble(6));
               lista.add(b);                
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    
    public List<Cliente> listarboletocliente() {
       //List<Boleto> lista=new ArrayList();
        List<Cliente> lista2 = new ArrayList();//
       String sql = "call listarboletocliente()";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next() == true) {//mientras haya listas pa leer
               /*Boleto b = new Boleto();
               b.setIdboleto(rs.getInt(1));
               lista.add(b*/
               Cliente c= new Cliente();
               c.setIdcliente(rs.getInt(2));
               c.setNombre(rs.getString(3));
               lista2.add(c);
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista2;
    }
    
    public int modificarboleto(int idb, Date fe, int pb, 
            String oficina,double valor, double descuento, int idc) {
        int sw = 0;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
        String sfe=dateFormat.format(fe);
        
        String sql = "call modificarboleto (?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idb);
            ps.setString(2, sfe);
            ps.setInt(3, pb);
            ps.setString(4, oficina);
            ps.setDouble(5, valor);
            ps.setDouble(6, descuento);
            ps.setInt(7, idc);
            
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public int eliminarboleto(int bol) {
        int sw = 0;
        
        String sql = "call eliminarboleto (?)";//UN SIGNO DE INTERROGACION PORQUE SOLO PIDE CODIGO, un parametro
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, bol);
            int nfilas = ps.executeUpdate();
            if (nfilas > 0) {
                sw = 1;
            }
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sw;
 }
    
    public Boleto buscarboleto(int cod) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Boleto b = new Boleto();//vacio
       
        String sql = "call buscarboleto (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cod);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              
              b.setIdboleto(rs.getInt(1));//rs lee codigo
               b.setFe(rs.getDate(2));//.... el objeto d que tiene codigo y nombre va llenar la lista
               b.setPb(rs.getInt(3));
               b.setOficina(rs.getString(4));
               b.setValor(rs.getDouble(5));
               b.setDescuento(rs.getDouble(6));
              
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return b;
    }
    
    public Cliente buscarboletocliente(int sid) {//modificarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
        Boleto b = new Boleto();
        Cliente c= new Cliente();
        String sql = "call buscarboletocliente (?)";
       try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, sid);
            ResultSet rs = ps.executeQuery();//para select usamos execute query, q nos devuelve conjunto de filas, la fila correspondiente al codigo
            if (rs.next() == true) {//si es true, encontro el codigo //asigno al p vacio los siguientes elementos... codigo, nombre, etc.
              b.setIdboleto(rs.getInt(1));
              c.setIdcliente(rs.getInt(2));
              c.setNombre(rs.getString(3));
              
              
              
            }              
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return c;
       
    }
}
